function t(t,e,i,s){var o,r=arguments.length,n=r<3?e:null===s?s=Object.getOwnPropertyDescriptor(e,i):s;if("object"==typeof Reflect&&"function"==typeof Reflect.decorate)n=Reflect.decorate(t,e,i,s);else for(var l=t.length-1;l>=0;l--)(o=t[l])&&(n=(r<3?o(n):r>3?o(e,i,n):o(e,i))||n);return r>3&&n&&Object.defineProperty(e,i,n),n
/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}const e=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,i=Symbol(),s=new Map;class o{constructor(t,e){if(this._$cssResult$=!0,e!==i)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){let t=s.get(this.cssText);return e&&void 0===t&&(s.set(this.cssText,t=new CSSStyleSheet),t.replaceSync(this.cssText)),t}toString(){return this.cssText}}const r=e?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const i of t.cssRules)e+=i.cssText;return(t=>new o("string"==typeof t?t:t+"",i))(e)})(t):t
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */;var n;const l=window.trustedTypes,a=l?l.emptyScript:"",d=window.reactiveElementPolyfillSupport,h={toAttribute(t,e){switch(e){case Boolean:t=t?a:null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let i=t;switch(e){case Boolean:i=null!==t;break;case Number:i=null===t?null:Number(t);break;case Object:case Array:try{i=JSON.parse(t)}catch(t){i=null}}return i}},c=(t,e)=>e!==t&&(e==e||t==t),p={attribute:!0,type:String,converter:h,reflect:!1,hasChanged:c};class u extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(t){var e;null!==(e=this.l)&&void 0!==e||(this.l=[]),this.l.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,i)=>{const s=this._$Eh(i,e);void 0!==s&&(this._$Eu.set(s,i),t.push(s))})),t}static createProperty(t,e=p){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const i="symbol"==typeof t?Symbol():"__"+t,s=this.getPropertyDescriptor(t,i,e);void 0!==s&&Object.defineProperty(this.prototype,t,s)}}static getPropertyDescriptor(t,e,i){return{get(){return this[e]},set(s){const o=this[t];this[e]=s,this.requestUpdate(t,o,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||p}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const i of e)this.createProperty(i,t[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const i=new Set(t.flat(1/0).reverse());for(const t of i)e.unshift(r(t))}else void 0!==t&&e.push(r(t));return e}static _$Eh(t,e){const i=e.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof t?t.toLowerCase():void 0}o(){var t;this._$Ep=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Em(),this.requestUpdate(),null===(t=this.constructor.l)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,i;(null!==(e=this._$Eg)&&void 0!==e?e:this._$Eg=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(i=t.hostConnected)||void 0===i||i.call(t))}removeController(t){var e;null===(e=this._$Eg)||void 0===e||e.splice(this._$Eg.indexOf(t)>>>0,1)}_$Em(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Et.set(e,this[e]),delete this[e])}))}createRenderRoot(){var t;const i=null!==(t=this.shadowRoot)&&void 0!==t?t:this.attachShadow(this.constructor.shadowRootOptions);return((t,i)=>{e?t.adoptedStyleSheets=i.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):i.forEach((e=>{const i=document.createElement("style"),s=window.litNonce;void 0!==s&&i.setAttribute("nonce",s),i.textContent=e.cssText,t.appendChild(i)}))})(i,this.constructor.elementStyles),i}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,i){this._$AK(t,i)}_$ES(t,e,i=p){var s,o;const r=this.constructor._$Eh(t,i);if(void 0!==r&&!0===i.reflect){const n=(null!==(o=null===(s=i.converter)||void 0===s?void 0:s.toAttribute)&&void 0!==o?o:h.toAttribute)(e,i.type);this._$Ei=t,null==n?this.removeAttribute(r):this.setAttribute(r,n),this._$Ei=null}}_$AK(t,e){var i,s,o;const r=this.constructor,n=r._$Eu.get(t);if(void 0!==n&&this._$Ei!==n){const t=r.getPropertyOptions(n),l=t.converter,a=null!==(o=null!==(s=null===(i=l)||void 0===i?void 0:i.fromAttribute)&&void 0!==s?s:"function"==typeof l?l:null)&&void 0!==o?o:h.fromAttribute;this._$Ei=n,this[n]=a(e,t.type),this._$Ei=null}}requestUpdate(t,e,i){let s=!0;void 0!==t&&(((i=i||this.constructor.getPropertyOptions(t)).hasChanged||c)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===i.reflect&&this._$Ei!==t&&(void 0===this._$EC&&(this._$EC=new Map),this._$EC.set(t,i))):s=!1),!this.isUpdatePending&&s&&(this._$Ep=this._$E_())}async _$E_(){this.isUpdatePending=!0;try{await this._$Ep}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((t,e)=>this[e]=t)),this._$Et=void 0);let e=!1;const i=this._$AL;try{e=this.shouldUpdate(i),e?(this.willUpdate(i),null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(i)):this._$EU()}catch(t){throw e=!1,this._$EU(),t}e&&this._$AE(i)}willUpdate(t){}_$AE(t){var e;null===(e=this._$Eg)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$EU(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ep}shouldUpdate(t){return!0}update(t){void 0!==this._$EC&&(this._$EC.forEach(((t,e)=>this._$ES(e,this[e],t))),this._$EC=void 0),this._$EU()}updated(t){}firstUpdated(t){}}
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var g;u.finalized=!0,u.elementProperties=new Map,u.elementStyles=[],u.shadowRootOptions={mode:"open"},null==d||d({ReactiveElement:u}),(null!==(n=globalThis.reactiveElementVersions)&&void 0!==n?n:globalThis.reactiveElementVersions=[]).push("1.3.2");const v=globalThis.trustedTypes,f=v?v.createPolicy("lit-html",{createHTML:t=>t}):void 0,$=`lit$${(Math.random()+"").slice(9)}$`,m="?"+$,y=`<${m}>`,_=document,A=(t="")=>_.createComment(t),b=t=>null===t||"object"!=typeof t&&"function"!=typeof t,w=Array.isArray,S=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,E=/-->/g,x=/>/g,C=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,T=/'/g,P=/"/g,U=/^(?:script|style|textarea|title)$/i,k=(t=>(e,...i)=>({_$litType$:t,strings:e,values:i}))(1),H=Symbol.for("lit-noChange"),R=Symbol.for("lit-nothing"),L=new WeakMap,M=_.createTreeWalker(_,129,null,!1);class N{constructor({strings:t,_$litType$:e},i){let s;this.parts=[];let o=0,r=0;const n=t.length-1,l=this.parts,[a,d]=((t,e)=>{const i=t.length-1,s=[];let o,r=2===e?"<svg>":"",n=S;for(let e=0;e<i;e++){const i=t[e];let l,a,d=-1,h=0;for(;h<i.length&&(n.lastIndex=h,a=n.exec(i),null!==a);)h=n.lastIndex,n===S?"!--"===a[1]?n=E:void 0!==a[1]?n=x:void 0!==a[2]?(U.test(a[2])&&(o=RegExp("</"+a[2],"g")),n=C):void 0!==a[3]&&(n=C):n===C?">"===a[0]?(n=null!=o?o:S,d=-1):void 0===a[1]?d=-2:(d=n.lastIndex-a[2].length,l=a[1],n=void 0===a[3]?C:'"'===a[3]?P:T):n===P||n===T?n=C:n===E||n===x?n=S:(n=C,o=void 0);const c=n===C&&t[e+1].startsWith("/>")?" ":"";r+=n===S?i+y:d>=0?(s.push(l),i.slice(0,d)+"$lit$"+i.slice(d)+$+c):i+$+(-2===d?(s.push(void 0),e):c)}const l=r+(t[i]||"<?>")+(2===e?"</svg>":"");if(!Array.isArray(t)||!t.hasOwnProperty("raw"))throw Error("invalid template strings array");return[void 0!==f?f.createHTML(l):l,s]})(t,e);if(this.el=N.createElement(a,i),M.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(s=M.nextNode())&&l.length<n;){if(1===s.nodeType){if(s.hasAttributes()){const t=[];for(const e of s.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith($)){const i=d[r++];if(t.push(e),void 0!==i){const t=s.getAttribute(i.toLowerCase()+"$lit$").split($),e=/([.?@])?(.*)/.exec(i);l.push({type:1,index:o,name:e[2],strings:t,ctor:"."===e[1]?j:"?"===e[1]?V:"@"===e[1]?W:B})}else l.push({type:6,index:o})}for(const e of t)s.removeAttribute(e)}if(U.test(s.tagName)){const t=s.textContent.split($),e=t.length-1;if(e>0){s.textContent=v?v.emptyScript:"";for(let i=0;i<e;i++)s.append(t[i],A()),M.nextNode(),l.push({type:2,index:++o});s.append(t[e],A())}}}else if(8===s.nodeType)if(s.data===m)l.push({type:2,index:o});else{let t=-1;for(;-1!==(t=s.data.indexOf($,t+1));)l.push({type:7,index:o}),t+=$.length-1}o++}}static createElement(t,e){const i=_.createElement("template");return i.innerHTML=t,i}}function O(t,e,i=t,s){var o,r,n,l;if(e===H)return e;let a=void 0!==s?null===(o=i._$Cl)||void 0===o?void 0:o[s]:i._$Cu;const d=b(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==d&&(null===(r=null==a?void 0:a._$AO)||void 0===r||r.call(a,!1),void 0===d?a=void 0:(a=new d(t),a._$AT(t,i,s)),void 0!==s?(null!==(n=(l=i)._$Cl)&&void 0!==n?n:l._$Cl=[])[s]=a:i._$Cu=a),void 0!==a&&(e=O(t,a._$AS(t,e.values),a,s)),e}class z{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:i},parts:s}=this._$AD,o=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:_).importNode(i,!0);M.currentNode=o;let r=M.nextNode(),n=0,l=0,a=s[0];for(;void 0!==a;){if(n===a.index){let e;2===a.type?e=new D(r,r.nextSibling,this,t):1===a.type?e=new a.ctor(r,a.name,a.strings,this,t):6===a.type&&(e=new q(r,this,t)),this.v.push(e),a=s[++l]}n!==(null==a?void 0:a.index)&&(r=M.nextNode(),n++)}return o}m(t){let e=0;for(const i of this.v)void 0!==i&&(void 0!==i.strings?(i._$AI(t,i,e),e+=i.strings.length-2):i._$AI(t[e])),e++}}class D{constructor(t,e,i,s){var o;this.type=2,this._$AH=R,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=i,this.options=s,this._$Cg=null===(o=null==s?void 0:s.isConnected)||void 0===o||o}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=O(this,t,e),b(t)?t===R||null==t||""===t?(this._$AH!==R&&this._$AR(),this._$AH=R):t!==this._$AH&&t!==H&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.k(t):(t=>{var e;return w(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])})(t)?this.S(t):this.$(t)}M(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}k(t){this._$AH!==t&&(this._$AR(),this._$AH=this.M(t))}$(t){this._$AH!==R&&b(this._$AH)?this._$AA.nextSibling.data=t:this.k(_.createTextNode(t)),this._$AH=t}T(t){var e;const{values:i,_$litType$:s}=t,o="number"==typeof s?this._$AC(t):(void 0===s.el&&(s.el=N.createElement(s.h,this.options)),s);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===o)this._$AH.m(i);else{const t=new z(o,this),e=t.p(this.options);t.m(i),this.k(e),this._$AH=t}}_$AC(t){let e=L.get(t.strings);return void 0===e&&L.set(t.strings,e=new N(t)),e}S(t){w(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let i,s=0;for(const o of t)s===e.length?e.push(i=new D(this.M(A()),this.M(A()),this,this.options)):i=e[s],i._$AI(o),s++;s<e.length&&(this._$AR(i&&i._$AB.nextSibling,s),e.length=s)}_$AR(t=this._$AA.nextSibling,e){var i;for(null===(i=this._$AP)||void 0===i||i.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class B{constructor(t,e,i,s,o){this.type=1,this._$AH=R,this._$AN=void 0,this.element=t,this.name=e,this._$AM=s,this.options=o,i.length>2||""!==i[0]||""!==i[1]?(this._$AH=Array(i.length-1).fill(new String),this.strings=i):this._$AH=R}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,i,s){const o=this.strings;let r=!1;if(void 0===o)t=O(this,t,e,0),r=!b(t)||t!==this._$AH&&t!==H,r&&(this._$AH=t);else{const s=t;let n,l;for(t=o[0],n=0;n<o.length-1;n++)l=O(this,s[i+n],e,n),l===H&&(l=this._$AH[n]),r||(r=!b(l)||l!==this._$AH[n]),l===R?t=R:t!==R&&(t+=(null!=l?l:"")+o[n+1]),this._$AH[n]=l}r&&!s&&this.C(t)}C(t){t===R?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class j extends B{constructor(){super(...arguments),this.type=3}C(t){this.element[this.name]=t===R?void 0:t}}const I=v?v.emptyScript:"";class V extends B{constructor(){super(...arguments),this.type=4}C(t){t&&t!==R?this.element.setAttribute(this.name,I):this.element.removeAttribute(this.name)}}class W extends B{constructor(t,e,i,s,o){super(t,e,i,s,o),this.type=5}_$AI(t,e=this){var i;if((t=null!==(i=O(this,t,e,0))&&void 0!==i?i:R)===H)return;const s=this._$AH,o=t===R&&s!==R||t.capture!==s.capture||t.once!==s.once||t.passive!==s.passive,r=t!==R&&(s===R||o);o&&this.element.removeEventListener(this.name,this,s),r&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,i;"function"==typeof this._$AH?this._$AH.call(null!==(i=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==i?i:this.element,t):this._$AH.handleEvent(t)}}class q{constructor(t,e,i){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=i}get _$AU(){return this._$AM._$AU}_$AI(t){O(this,t)}}const F=window.litHtmlPolyfillSupport;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var J,K;null==F||F(N,D),(null!==(g=globalThis.litHtmlVersions)&&void 0!==g?g:globalThis.litHtmlVersions=[]).push("2.2.6");class G extends u{constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var t,e;const i=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=i.firstChild),i}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Dt=((t,e,i)=>{var s,o;const r=null!==(s=null==i?void 0:i.renderBefore)&&void 0!==s?s:e;let n=r._$litPart$;if(void 0===n){const t=null!==(o=null==i?void 0:i.renderBefore)&&void 0!==o?o:null;r._$litPart$=n=new D(e.insertBefore(A(),t),t,void 0,null!=i?i:{})}return n._$AI(t),n})(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!1)}render(){return H}}G.finalized=!0,G._$litElement$=!0,null===(J=globalThis.litElementHydrateSupport)||void 0===J||J.call(globalThis,{LitElement:G});const Z=globalThis.litElementPolyfillSupport;null==Z||Z({LitElement:G}),(null!==(K=globalThis.litElementVersions)&&void 0!==K?K:globalThis.litElementVersions=[]).push("3.2.0");
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const Q=(t,e)=>"method"===e.kind&&e.descriptor&&!("value"in e.descriptor)?{...e,finisher(i){i.createProperty(e.key,t)}}:{kind:"field",key:Symbol(),placement:"own",descriptor:{},originalKey:e.key,initializer(){"function"==typeof e.initializer&&(this[e.key]=e.initializer.call(this))},finisher(i){i.createProperty(e.key,t)}};function X(t){return(e,i)=>void 0!==i?((t,e,i)=>{e.constructor.createProperty(i,t)})(t,e,i):Q(t,e)
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}var Y;null===(Y=window.HTMLSlotElement)||void 0===Y||Y.prototype.assignedElements;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const tt=2;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
class et extends class{constructor(t){}get _$AU(){return this._$AM._$AU}_$AT(t,e,i){this._$Ct=t,this._$AM=e,this._$Ci=i}_$AS(t,e){return this.update(t,e)}update(t,e){return this.render(...e)}}{constructor(t){if(super(t),this.it=R,t.type!==tt)throw Error(this.constructor.directiveName+"() can only be used in child bindings")}render(t){if(t===R||null==t)return this.ft=void 0,this.it=t;if(t===H)return t;if("string"!=typeof t)throw Error(this.constructor.directiveName+"() called with a non-string value");if(t===this.it)return this.ft;this.it=t;const e=[t];return e.raw=e,this.ft={_$litType$:this.constructor.resultType,strings:e,values:[]}}}et.directiveName="unsafeHTML",et.resultType=1;const it=(t=>(...e)=>({_$litDirective$:t,values:e}))(et),st="",ot="",rt="",nt=!1;class lt extends G{constructor(){super(...arguments),this.dim=st,this.slotStyle=ot,this.cellStyle=ot,this.rowStyle=rt,this.noFillers=nt,this.slotCounter=0}makeGridDim(){let t="",e="";const i=this.dim.split(/[;]/);""===i.at(-1)&&i.pop();let s=!1;if(i.length>0&&1===i.length&&1===i[0].split(" ").length&&(s=!0),!s){const s=i.length;for(let o=0;o<s;o++){const s=i[o].split(" ").filter((t=>t));let r="";for(let t=0;t<s.length;t++)s[t]=`${s[t]}%`,this.noFillers?r+=`\n             <div style="${this.slotStyle} ${this.cellStyle}">\n                <slot name="${this.slotCounter+1}"> </slot>\n            </div>`:r+=`\n             <div style="${this.slotStyle} ${this.cellStyle}">\n                <slot name="${this.slotCounter+1}">\n                  <i>replace me with: <br>\n                  <code>\n                  &lt;div slot=${this.slotCounter+1}&gt;your content&lt;/div&gt;\n                  </code>\n                  </i>\n                </slot>\n            </div>`,this.slotCounter++;t+=`\n          .grid-${o+1}{\n            display: grid;\n            grid-template-columns: ${s.join(" ")};\n            grid-gap: var(--gridspace-col);\n          }\n          `,e+=`\n        <div class="gridrow grid-${o+1}" style="${this.rowStyle}">\n          ${r}\n        </div>\n        `}}return`\n      ${`\n      <style>\n        ${t}\n      </style>\n    `}\n      ${`\n        ${e}\n    `}\n    `}render(){return this.getAttribute("dim")?k` ${it(this.makeGridDim())} `:k`
      <i>
        <p>
          [WARN] no grid dimensions specified. Make sure to include a
          <b>non-empty</b> <code>dim</code> attribute.
        </p>
      </i>
    `}}function at(t,e,i,s){var o,r=arguments.length,n=r<3?e:null===s?s=Object.getOwnPropertyDescriptor(e,i):s;if("object"==typeof Reflect&&"function"==typeof Reflect.decorate)n=Reflect.decorate(t,e,i,s);else for(var l=t.length-1;l>=0;l--)(o=t[l])&&(n=(r<3?o(n):r>3?o(e,i,n):o(e,i))||n);return r>3&&n&&Object.defineProperty(e,i,n),n
/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}lt.styles=((t,...e)=>{const s=1===t.length?t[0]:e.reduce(((e,i,s)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(i)+t[s+1]),t[0]);return new o(s,i)})`
    :host {
      --gridspace-row: var(--dd-gridspace-row, 10px);
      --gridspace-col: var(--dd-gridspace-col, 10px);
    }
    .gridrow {
      padding-bottom: var(--gridspace-row);
    }
  `,t([X({type:String,attribute:"dim"})],lt.prototype,"dim",void 0),t([X({type:String,attribute:"slot-style"})],lt.prototype,"slotStyle",void 0),t([X({type:String,attribute:"cell-style"})],lt.prototype,"cellStyle",void 0),t([X({type:String,attribute:"row-style"})],lt.prototype,"rowStyle",void 0),t([X({type:Boolean,attribute:"no-fillers",reflect:!0})],lt.prototype,"noFillers",void 0),t([X({type:Number})],lt.prototype,"slotCounter",void 0),window.customElements.define("dd-grid",lt);const dt=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,ht=Symbol(),ct=new Map;class pt{constructor(t,e){if(this._$cssResult$=!0,e!==ht)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){let t=ct.get(this.cssText);return dt&&void 0===t&&(ct.set(this.cssText,t=new CSSStyleSheet),t.replaceSync(this.cssText)),t}toString(){return this.cssText}}const ut=dt?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const i of t.cssRules)e+=i.cssText;return(t=>new pt("string"==typeof t?t:t+"",ht))(e)})(t):t
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */;var gt;const vt=window.trustedTypes,ft=vt?vt.emptyScript:"",$t=window.reactiveElementPolyfillSupport,mt={toAttribute(t,e){switch(e){case Boolean:t=t?ft:null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let i=t;switch(e){case Boolean:i=null!==t;break;case Number:i=null===t?null:Number(t);break;case Object:case Array:try{i=JSON.parse(t)}catch(t){i=null}}return i}},yt=(t,e)=>e!==t&&(e==e||t==t),_t={attribute:!0,type:String,converter:mt,reflect:!1,hasChanged:yt};class At extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(t){var e;null!==(e=this.l)&&void 0!==e||(this.l=[]),this.l.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,i)=>{const s=this._$Eh(i,e);void 0!==s&&(this._$Eu.set(s,i),t.push(s))})),t}static createProperty(t,e=_t){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const i="symbol"==typeof t?Symbol():"__"+t,s=this.getPropertyDescriptor(t,i,e);void 0!==s&&Object.defineProperty(this.prototype,t,s)}}static getPropertyDescriptor(t,e,i){return{get(){return this[e]},set(s){const o=this[t];this[e]=s,this.requestUpdate(t,o,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||_t}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const i of e)this.createProperty(i,t[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const i=new Set(t.flat(1/0).reverse());for(const t of i)e.unshift(ut(t))}else void 0!==t&&e.push(ut(t));return e}static _$Eh(t,e){const i=e.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof t?t.toLowerCase():void 0}o(){var t;this._$Ep=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Em(),this.requestUpdate(),null===(t=this.constructor.l)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,i;(null!==(e=this._$Eg)&&void 0!==e?e:this._$Eg=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(i=t.hostConnected)||void 0===i||i.call(t))}removeController(t){var e;null===(e=this._$Eg)||void 0===e||e.splice(this._$Eg.indexOf(t)>>>0,1)}_$Em(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Et.set(e,this[e]),delete this[e])}))}createRenderRoot(){var t;const e=null!==(t=this.shadowRoot)&&void 0!==t?t:this.attachShadow(this.constructor.shadowRootOptions);return((t,e)=>{dt?t.adoptedStyleSheets=e.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):e.forEach((e=>{const i=document.createElement("style"),s=window.litNonce;void 0!==s&&i.setAttribute("nonce",s),i.textContent=e.cssText,t.appendChild(i)}))})(e,this.constructor.elementStyles),e}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,i){this._$AK(t,i)}_$ES(t,e,i=_t){var s,o;const r=this.constructor._$Eh(t,i);if(void 0!==r&&!0===i.reflect){const n=(null!==(o=null===(s=i.converter)||void 0===s?void 0:s.toAttribute)&&void 0!==o?o:mt.toAttribute)(e,i.type);this._$Ei=t,null==n?this.removeAttribute(r):this.setAttribute(r,n),this._$Ei=null}}_$AK(t,e){var i,s,o;const r=this.constructor,n=r._$Eu.get(t);if(void 0!==n&&this._$Ei!==n){const t=r.getPropertyOptions(n),l=t.converter,a=null!==(o=null!==(s=null===(i=l)||void 0===i?void 0:i.fromAttribute)&&void 0!==s?s:"function"==typeof l?l:null)&&void 0!==o?o:mt.fromAttribute;this._$Ei=n,this[n]=a(e,t.type),this._$Ei=null}}requestUpdate(t,e,i){let s=!0;void 0!==t&&(((i=i||this.constructor.getPropertyOptions(t)).hasChanged||yt)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===i.reflect&&this._$Ei!==t&&(void 0===this._$EC&&(this._$EC=new Map),this._$EC.set(t,i))):s=!1),!this.isUpdatePending&&s&&(this._$Ep=this._$E_())}async _$E_(){this.isUpdatePending=!0;try{await this._$Ep}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((t,e)=>this[e]=t)),this._$Et=void 0);let e=!1;const i=this._$AL;try{e=this.shouldUpdate(i),e?(this.willUpdate(i),null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(i)):this._$EU()}catch(t){throw e=!1,this._$EU(),t}e&&this._$AE(i)}willUpdate(t){}_$AE(t){var e;null===(e=this._$Eg)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$EU(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ep}shouldUpdate(t){return!0}update(t){void 0!==this._$EC&&(this._$EC.forEach(((t,e)=>this._$ES(e,this[e],t))),this._$EC=void 0),this._$EU()}updated(t){}firstUpdated(t){}}
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var bt;At.finalized=!0,At.elementProperties=new Map,At.elementStyles=[],At.shadowRootOptions={mode:"open"},null==$t||$t({ReactiveElement:At}),(null!==(gt=globalThis.reactiveElementVersions)&&void 0!==gt?gt:globalThis.reactiveElementVersions=[]).push("1.3.2");const wt=globalThis.trustedTypes,St=wt?wt.createPolicy("lit-html",{createHTML:t=>t}):void 0,Et=`lit$${(Math.random()+"").slice(9)}$`,xt="?"+Et,Ct=`<${xt}>`,Tt=document,Pt=(t="")=>Tt.createComment(t),Ut=t=>null===t||"object"!=typeof t&&"function"!=typeof t,kt=Array.isArray,Ht=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,Rt=/-->/g,Lt=/>/g,Mt=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,Nt=/'/g,Ot=/"/g,zt=/^(?:script|style|textarea|title)$/i,Dt=(t=>(e,...i)=>({_$litType$:t,strings:e,values:i}))(1),Bt=Symbol.for("lit-noChange"),jt=Symbol.for("lit-nothing"),It=new WeakMap,Vt=Tt.createTreeWalker(Tt,129,null,!1);class Wt{constructor({strings:t,_$litType$:e},i){let s;this.parts=[];let o=0,r=0;const n=t.length-1,l=this.parts,[a,d]=((t,e)=>{const i=t.length-1,s=[];let o,r=2===e?"<svg>":"",n=Ht;for(let e=0;e<i;e++){const i=t[e];let l,a,d=-1,h=0;for(;h<i.length&&(n.lastIndex=h,a=n.exec(i),null!==a);)h=n.lastIndex,n===Ht?"!--"===a[1]?n=Rt:void 0!==a[1]?n=Lt:void 0!==a[2]?(zt.test(a[2])&&(o=RegExp("</"+a[2],"g")),n=Mt):void 0!==a[3]&&(n=Mt):n===Mt?">"===a[0]?(n=null!=o?o:Ht,d=-1):void 0===a[1]?d=-2:(d=n.lastIndex-a[2].length,l=a[1],n=void 0===a[3]?Mt:'"'===a[3]?Ot:Nt):n===Ot||n===Nt?n=Mt:n===Rt||n===Lt?n=Ht:(n=Mt,o=void 0);const c=n===Mt&&t[e+1].startsWith("/>")?" ":"";r+=n===Ht?i+Ct:d>=0?(s.push(l),i.slice(0,d)+"$lit$"+i.slice(d)+Et+c):i+Et+(-2===d?(s.push(void 0),e):c)}const l=r+(t[i]||"<?>")+(2===e?"</svg>":"");if(!Array.isArray(t)||!t.hasOwnProperty("raw"))throw Error("invalid template strings array");return[void 0!==St?St.createHTML(l):l,s]})(t,e);if(this.el=Wt.createElement(a,i),Vt.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(s=Vt.nextNode())&&l.length<n;){if(1===s.nodeType){if(s.hasAttributes()){const t=[];for(const e of s.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith(Et)){const i=d[r++];if(t.push(e),void 0!==i){const t=s.getAttribute(i.toLowerCase()+"$lit$").split(Et),e=/([.?@])?(.*)/.exec(i);l.push({type:1,index:o,name:e[2],strings:t,ctor:"."===e[1]?Gt:"?"===e[1]?Qt:"@"===e[1]?Xt:Kt})}else l.push({type:6,index:o})}for(const e of t)s.removeAttribute(e)}if(zt.test(s.tagName)){const t=s.textContent.split(Et),e=t.length-1;if(e>0){s.textContent=wt?wt.emptyScript:"";for(let i=0;i<e;i++)s.append(t[i],Pt()),Vt.nextNode(),l.push({type:2,index:++o});s.append(t[e],Pt())}}}else if(8===s.nodeType)if(s.data===xt)l.push({type:2,index:o});else{let t=-1;for(;-1!==(t=s.data.indexOf(Et,t+1));)l.push({type:7,index:o}),t+=Et.length-1}o++}}static createElement(t,e){const i=Tt.createElement("template");return i.innerHTML=t,i}}function qt(t,e,i=t,s){var o,r,n,l;if(e===Bt)return e;let a=void 0!==s?null===(o=i._$Cl)||void 0===o?void 0:o[s]:i._$Cu;const d=Ut(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==d&&(null===(r=null==a?void 0:a._$AO)||void 0===r||r.call(a,!1),void 0===d?a=void 0:(a=new d(t),a._$AT(t,i,s)),void 0!==s?(null!==(n=(l=i)._$Cl)&&void 0!==n?n:l._$Cl=[])[s]=a:i._$Cu=a),void 0!==a&&(e=qt(t,a._$AS(t,e.values),a,s)),e}class Ft{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:i},parts:s}=this._$AD,o=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:Tt).importNode(i,!0);Vt.currentNode=o;let r=Vt.nextNode(),n=0,l=0,a=s[0];for(;void 0!==a;){if(n===a.index){let e;2===a.type?e=new Jt(r,r.nextSibling,this,t):1===a.type?e=new a.ctor(r,a.name,a.strings,this,t):6===a.type&&(e=new Yt(r,this,t)),this.v.push(e),a=s[++l]}n!==(null==a?void 0:a.index)&&(r=Vt.nextNode(),n++)}return o}m(t){let e=0;for(const i of this.v)void 0!==i&&(void 0!==i.strings?(i._$AI(t,i,e),e+=i.strings.length-2):i._$AI(t[e])),e++}}class Jt{constructor(t,e,i,s){var o;this.type=2,this._$AH=jt,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=i,this.options=s,this._$Cg=null===(o=null==s?void 0:s.isConnected)||void 0===o||o}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=qt(this,t,e),Ut(t)?t===jt||null==t||""===t?(this._$AH!==jt&&this._$AR(),this._$AH=jt):t!==this._$AH&&t!==Bt&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.k(t):(t=>{var e;return kt(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])})(t)?this.S(t):this.$(t)}M(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}k(t){this._$AH!==t&&(this._$AR(),this._$AH=this.M(t))}$(t){this._$AH!==jt&&Ut(this._$AH)?this._$AA.nextSibling.data=t:this.k(Tt.createTextNode(t)),this._$AH=t}T(t){var e;const{values:i,_$litType$:s}=t,o="number"==typeof s?this._$AC(t):(void 0===s.el&&(s.el=Wt.createElement(s.h,this.options)),s);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===o)this._$AH.m(i);else{const t=new Ft(o,this),e=t.p(this.options);t.m(i),this.k(e),this._$AH=t}}_$AC(t){let e=It.get(t.strings);return void 0===e&&It.set(t.strings,e=new Wt(t)),e}S(t){kt(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let i,s=0;for(const o of t)s===e.length?e.push(i=new Jt(this.M(Pt()),this.M(Pt()),this,this.options)):i=e[s],i._$AI(o),s++;s<e.length&&(this._$AR(i&&i._$AB.nextSibling,s),e.length=s)}_$AR(t=this._$AA.nextSibling,e){var i;for(null===(i=this._$AP)||void 0===i||i.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class Kt{constructor(t,e,i,s,o){this.type=1,this._$AH=jt,this._$AN=void 0,this.element=t,this.name=e,this._$AM=s,this.options=o,i.length>2||""!==i[0]||""!==i[1]?(this._$AH=Array(i.length-1).fill(new String),this.strings=i):this._$AH=jt}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,i,s){const o=this.strings;let r=!1;if(void 0===o)t=qt(this,t,e,0),r=!Ut(t)||t!==this._$AH&&t!==Bt,r&&(this._$AH=t);else{const s=t;let n,l;for(t=o[0],n=0;n<o.length-1;n++)l=qt(this,s[i+n],e,n),l===Bt&&(l=this._$AH[n]),r||(r=!Ut(l)||l!==this._$AH[n]),l===jt?t=jt:t!==jt&&(t+=(null!=l?l:"")+o[n+1]),this._$AH[n]=l}r&&!s&&this.C(t)}C(t){t===jt?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class Gt extends Kt{constructor(){super(...arguments),this.type=3}C(t){this.element[this.name]=t===jt?void 0:t}}const Zt=wt?wt.emptyScript:"";class Qt extends Kt{constructor(){super(...arguments),this.type=4}C(t){t&&t!==jt?this.element.setAttribute(this.name,Zt):this.element.removeAttribute(this.name)}}class Xt extends Kt{constructor(t,e,i,s,o){super(t,e,i,s,o),this.type=5}_$AI(t,e=this){var i;if((t=null!==(i=qt(this,t,e,0))&&void 0!==i?i:jt)===Bt)return;const s=this._$AH,o=t===jt&&s!==jt||t.capture!==s.capture||t.once!==s.once||t.passive!==s.passive,r=t!==jt&&(s===jt||o);o&&this.element.removeEventListener(this.name,this,s),r&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,i;"function"==typeof this._$AH?this._$AH.call(null!==(i=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==i?i:this.element,t):this._$AH.handleEvent(t)}}class Yt{constructor(t,e,i){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=i}get _$AU(){return this._$AM._$AU}_$AI(t){qt(this,t)}}const te=window.litHtmlPolyfillSupport;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var ee,ie;null==te||te(Wt,Jt),(null!==(bt=globalThis.litHtmlVersions)&&void 0!==bt?bt:globalThis.litHtmlVersions=[]).push("2.2.6");class se extends At{constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var t,e;const i=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=i.firstChild),i}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Dt=((t,e,i)=>{var s,o;const r=null!==(s=null==i?void 0:i.renderBefore)&&void 0!==s?s:e;let n=r._$litPart$;if(void 0===n){const t=null!==(o=null==i?void 0:i.renderBefore)&&void 0!==o?o:null;r._$litPart$=n=new Jt(e.insertBefore(Pt(),t),t,void 0,null!=i?i:{})}return n._$AI(t),n})(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!1)}render(){return Bt}}se.finalized=!0,se._$litElement$=!0,null===(ee=globalThis.litElementHydrateSupport)||void 0===ee||ee.call(globalThis,{LitElement:se});const oe=globalThis.litElementPolyfillSupport;null==oe||oe({LitElement:se}),(null!==(ie=globalThis.litElementVersions)&&void 0!==ie?ie:globalThis.litElementVersions=[]).push("3.2.0");
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const re=(t,e)=>"method"===e.kind&&e.descriptor&&!("value"in e.descriptor)?{...e,finisher(i){i.createProperty(e.key,t)}}:{kind:"field",key:Symbol(),placement:"own",descriptor:{},originalKey:e.key,initializer(){"function"==typeof e.initializer&&(this[e.key]=e.initializer.call(this))},finisher(i){i.createProperty(e.key,t)}};function ne(t){return(e,i)=>void 0!==i?((t,e,i)=>{e.constructor.createProperty(i,t)})(t,e,i):re(t,e)
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}var le;null===(le=window.HTMLSlotElement)||void 0===le||le.prototype.assignedElements;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const ae=2;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
class de extends class{constructor(t){}get _$AU(){return this._$AM._$AU}_$AT(t,e,i){this._$Ct=t,this._$AM=e,this._$Ci=i}_$AS(t,e){return this.update(t,e)}update(t,e){return this.render(...e)}}{constructor(t){if(super(t),this.it=jt,t.type!==ae)throw Error(this.constructor.directiveName+"() can only be used in child bindings")}render(t){if(t===jt||null==t)return this.ft=void 0,this.it=t;if(t===Bt)return t;if("string"!=typeof t)throw Error(this.constructor.directiveName+"() called with a non-string value");if(t===this.it)return this.ft;this.it=t;const e=[t];return e.raw=e,this.ft={_$litType$:this.constructor.resultType,strings:e,values:[]}}}de.directiveName="unsafeHTML",de.resultType=1;const he=(t=>(...e)=>({_$litDirective$:t,values:e}))(de),ce="",pe="",ue="",ge=!1,ve=!1;class fe extends se{constructor(){super(...arguments),this.dim=ce,this.slotStyle=pe,this.cellStyle=pe,this.rowStyle=ue,this.noFillers=ge,this.noFooter=ve,this.slotCounter=0,this._checkFooter=()=>{if(this.noFooter){this.querySelector("dd-footer").style.display="none"}}}_makeGridDim(){let t="",e="";const i=this.dim.split(/[;]/);""===i.at(-1)&&i.pop();let s=!1;if(i.length>0&&1===i.length&&1===i[0].split(" ").length&&(s=!0),!s){const s=i.length;for(let o=0;o<s;o++){const s=i[o].split(" ").filter((t=>t));let r="";for(let t=0;t<s.length;t++)s[t]=`${s[t]}%`,this.noFillers?r+=`\n             <div style="${this.slotStyle} ${this.cellStyle}">\n                <slot name="${this.slotCounter+1}"> </slot>\n            </div>`:r+=`\n             <div style="${this.slotStyle} ${this.cellStyle}">\n                <slot name="${this.slotCounter+1}">\n                  <i>replace me with: <br>\n                  <code>\n                  &lt;div slot=${this.slotCounter+1}&gt;your content&lt;/div&gt;\n                  </code>\n                  </i>\n                </slot>\n            </div>`,this.slotCounter++;t+=`\n          .grid-${o+1}{\n            display: grid;\n            grid-template-columns: ${s.join(" ")};\n            grid-gap: var(--slide-gridspace-col);\n          }\n          `,e+=`\n        <div class="gridrow grid-${o+1}" style="${this.rowStyle}">\n          ${r}\n        </div>\n        `}}return`\n      ${`\n      <style>\n        ${t}\n      </style>\n    `}\n      ${`\n        ${e}\n    `}\n    `}async firstUpdated(){}connectedCallback(){super.connectedCallback(),document.addEventListener("DOMContentLoaded",this._checkFooter)}disconnectedCallback(){window.removeEventListener("DOMContentLoaded",this._checkFooter),super.disconnectedCallback()}render(){return this.classList.add("slide"),this.title="Slide",this.dim?this.noFillers?Dt`
        <slot></slot>
        ${he(this._makeGridDim())}
        <slot name="postgrid"></slot>
      `:Dt`
          <slot>
            <h2>Put a title to remove me</h2>
          </slot>
          ${he(this._makeGridDim())}
          <slot name="postgrid"></slot>
        `:Dt`
      <slot
        ><h2>
          No content added, or no grid layout defined. Default will be an empty
          page. Typing content in your slide will replace this message.
        </h2></slot
      >
    `}}function $e(t,e,i,s){var o,r=arguments.length,n=r<3?e:null===s?s=Object.getOwnPropertyDescriptor(e,i):s;if("object"==typeof Reflect&&"function"==typeof Reflect.decorate)n=Reflect.decorate(t,e,i,s);else for(var l=t.length-1;l>=0;l--)(o=t[l])&&(n=(r<3?o(n):r>3?o(e,i,n):o(e,i))||n);return r>3&&n&&Object.defineProperty(e,i,n),n
/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}fe.styles=((t,...e)=>{const i=1===t.length?t[0]:e.reduce(((e,i,s)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(i)+t[s+1]),t[0]);return new pt(i,ht)})`
    /****************************************************************
     * Element styling
     ****************************************************************/

    :host {
      /* color pallette */
      --slide-color-heading: var(--dd-color-heading, var(--dd-color-prim-dark));
      --slide-color-link: var(--dd-color-link, rgba(65, 90, 72, 0.7));
      --slide-color-link-hover: var(--dd-color-link-hover, rgba(65, 90, 72, 1));

      --slide-ratio: var(--dd-slide-ratio, calc(16 / 9));
      --slide-width: var(--dd-slide-width, 1024px);
      --slide-height: calc(var(--slide-width) / var(--slide-ratio));
      --slide-font: var(--dd-font, 24px/2 'Roboto', sans-serif);

      --slide-pad-top: 0px;
      --slide-pad-top-content: 0px;
      --slide-pad-right: calc(25px + var(--slide-gridspace-col));
      --slide-pad-left: 25px;

      --slide-gridspace-row: var(--dd-slide-gridspace-row, 10px);
      --slide-gridspace-col: var(--dd-slide-gridspace-col, 10px);

      display: block;
      font: var(--slide-font);
    }

    ::slotted(h1),
    h1,
    ::slotted(h2),
    h2,
    ::slotted(h3),
    h3,
    ::slotted(h4),
    h4,
    ::slotted(h5),
    h5,
    ::slotted(h6),
    h6 {
      /* style headings in the template "<slot> dummy text </slot>" */
      color: var(--slide-color-heading);
      margin: 0;
      padding: 0 0 var(--slide-pad-top-content) 0;
    }

    :host(.slide) {
      padding: var(--slide-pad-top) var(--slide-pad-right) 0
        var(--slide-pad-left);

      position: relative;
      z-index: 0;
      overflow: hidden;
      box-sizing: border-box;
      width: var(--slide-width);
      max-width: 100%;
      height: var(--slide-height);
      background-color: white;
      margin: 0;
    }

    .gridrow {
      padding-bottom: var(--slide-gridspace-row);
    }

    /* https://github.com/WICG/webcomponents/issues/889 */
  `,at([ne({type:String,attribute:"dim"})],fe.prototype,"dim",void 0),at([ne({type:String,attribute:"slot-style"})],fe.prototype,"slotStyle",void 0),at([ne({type:String,attribute:"cell-style"})],fe.prototype,"cellStyle",void 0),at([ne({type:String,attribute:"row-style"})],fe.prototype,"rowStyle",void 0),at([ne({type:Boolean,attribute:"no-fillers",reflect:!0})],fe.prototype,"noFillers",void 0),at([ne({type:Boolean,attribute:"no-footer",reflect:!0})],fe.prototype,"noFooter",void 0),at([ne({type:Number})],fe.prototype,"slotCounter",void 0),window.customElements.define("dd-slide",fe);const me=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,ye=Symbol(),_e=new WeakMap;class Ae{constructor(t,e,i){if(this._$cssResult$=!0,i!==ye)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t,this.t=e}get styleSheet(){let t=this.o;const e=this.t;if(me&&void 0===t){const i=void 0!==e&&1===e.length;i&&(t=_e.get(e)),void 0===t&&((this.o=t=new CSSStyleSheet).replaceSync(this.cssText),i&&_e.set(e,t))}return t}toString(){return this.cssText}}const be=me?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const i of t.cssRules)e+=i.cssText;return(t=>new Ae("string"==typeof t?t:t+"",void 0,ye))(e)})(t):t
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */;var we;const Se=window.trustedTypes,Ee=Se?Se.emptyScript:"",xe=window.reactiveElementPolyfillSupport,Ce={toAttribute(t,e){switch(e){case Boolean:t=t?Ee:null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let i=t;switch(e){case Boolean:i=null!==t;break;case Number:i=null===t?null:Number(t);break;case Object:case Array:try{i=JSON.parse(t)}catch(t){i=null}}return i}},Te=(t,e)=>e!==t&&(e==e||t==t),Pe={attribute:!0,type:String,converter:Ce,reflect:!1,hasChanged:Te};class Ue extends HTMLElement{constructor(){super(),this._$Ei=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$El=null,this.u()}static addInitializer(t){var e;null!==(e=this.h)&&void 0!==e||(this.h=[]),this.h.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,i)=>{const s=this._$Ep(i,e);void 0!==s&&(this._$Ev.set(s,i),t.push(s))})),t}static createProperty(t,e=Pe){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const i="symbol"==typeof t?Symbol():"__"+t,s=this.getPropertyDescriptor(t,i,e);void 0!==s&&Object.defineProperty(this.prototype,t,s)}}static getPropertyDescriptor(t,e,i){return{get(){return this[e]},set(s){const o=this[t];this[e]=s,this.requestUpdate(t,o,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||Pe}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Ev=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const i of e)this.createProperty(i,t[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const i=new Set(t.flat(1/0).reverse());for(const t of i)e.unshift(be(t))}else void 0!==t&&e.push(be(t));return e}static _$Ep(t,e){const i=e.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof t?t.toLowerCase():void 0}u(){var t;this._$E_=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Eg(),this.requestUpdate(),null===(t=this.constructor.h)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,i;(null!==(e=this._$ES)&&void 0!==e?e:this._$ES=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(i=t.hostConnected)||void 0===i||i.call(t))}removeController(t){var e;null===(e=this._$ES)||void 0===e||e.splice(this._$ES.indexOf(t)>>>0,1)}_$Eg(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Ei.set(e,this[e]),delete this[e])}))}createRenderRoot(){var t;const e=null!==(t=this.shadowRoot)&&void 0!==t?t:this.attachShadow(this.constructor.shadowRootOptions);return((t,e)=>{me?t.adoptedStyleSheets=e.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):e.forEach((e=>{const i=document.createElement("style"),s=window.litNonce;void 0!==s&&i.setAttribute("nonce",s),i.textContent=e.cssText,t.appendChild(i)}))})(e,this.constructor.elementStyles),e}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$ES)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$ES)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,i){this._$AK(t,i)}_$EO(t,e,i=Pe){var s,o;const r=this.constructor._$Ep(t,i);if(void 0!==r&&!0===i.reflect){const n=(null!==(o=null===(s=i.converter)||void 0===s?void 0:s.toAttribute)&&void 0!==o?o:Ce.toAttribute)(e,i.type);this._$El=t,null==n?this.removeAttribute(r):this.setAttribute(r,n),this._$El=null}}_$AK(t,e){var i,s;const o=this.constructor,r=o._$Ev.get(t);if(void 0!==r&&this._$El!==r){const t=o.getPropertyOptions(r),n=t.converter,l=null!==(s=null!==(i=null==n?void 0:n.fromAttribute)&&void 0!==i?i:"function"==typeof n?n:null)&&void 0!==s?s:Ce.fromAttribute;this._$El=r,this[r]=l(e,t.type),this._$El=null}}requestUpdate(t,e,i){let s=!0;void 0!==t&&(((i=i||this.constructor.getPropertyOptions(t)).hasChanged||Te)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===i.reflect&&this._$El!==t&&(void 0===this._$EC&&(this._$EC=new Map),this._$EC.set(t,i))):s=!1),!this.isUpdatePending&&s&&(this._$E_=this._$Ej())}async _$Ej(){this.isUpdatePending=!0;try{await this._$E_}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Ei&&(this._$Ei.forEach(((t,e)=>this[e]=t)),this._$Ei=void 0);let e=!1;const i=this._$AL;try{e=this.shouldUpdate(i),e?(this.willUpdate(i),null===(t=this._$ES)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(i)):this._$Ek()}catch(t){throw e=!1,this._$Ek(),t}e&&this._$AE(i)}willUpdate(t){}_$AE(t){var e;null===(e=this._$ES)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$Ek(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$E_}shouldUpdate(t){return!0}update(t){void 0!==this._$EC&&(this._$EC.forEach(((t,e)=>this._$EO(e,this[e],t))),this._$EC=void 0),this._$Ek()}updated(t){}firstUpdated(t){}}
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var ke;Ue.finalized=!0,Ue.elementProperties=new Map,Ue.elementStyles=[],Ue.shadowRootOptions={mode:"open"},null==xe||xe({ReactiveElement:Ue}),(null!==(we=globalThis.reactiveElementVersions)&&void 0!==we?we:globalThis.reactiveElementVersions=[]).push("1.3.3");const He=globalThis.trustedTypes,Re=He?He.createPolicy("lit-html",{createHTML:t=>t}):void 0,Le=`lit$${(Math.random()+"").slice(9)}$`,Me="?"+Le,Ne=`<${Me}>`,Oe=document,ze=(t="")=>Oe.createComment(t),De=t=>null===t||"object"!=typeof t&&"function"!=typeof t,Be=Array.isArray,je=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,Ie=/-->/g,Ve=/>/g,We=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,qe=/'/g,Fe=/"/g,Je=/^(?:script|style|textarea|title)$/i,Ke=(t=>(e,...i)=>({_$litType$:t,strings:e,values:i}))(1),Ge=Symbol.for("lit-noChange"),Ze=Symbol.for("lit-nothing"),Qe=new WeakMap,Xe=Oe.createTreeWalker(Oe,129,null,!1);class Ye{constructor({strings:t,_$litType$:e},i){let s;this.parts=[];let o=0,r=0;const n=t.length-1,l=this.parts,[a,d]=((t,e)=>{const i=t.length-1,s=[];let o,r=2===e?"<svg>":"",n=je;for(let e=0;e<i;e++){const i=t[e];let l,a,d=-1,h=0;for(;h<i.length&&(n.lastIndex=h,a=n.exec(i),null!==a);)h=n.lastIndex,n===je?"!--"===a[1]?n=Ie:void 0!==a[1]?n=Ve:void 0!==a[2]?(Je.test(a[2])&&(o=RegExp("</"+a[2],"g")),n=We):void 0!==a[3]&&(n=We):n===We?">"===a[0]?(n=null!=o?o:je,d=-1):void 0===a[1]?d=-2:(d=n.lastIndex-a[2].length,l=a[1],n=void 0===a[3]?We:'"'===a[3]?Fe:qe):n===Fe||n===qe?n=We:n===Ie||n===Ve?n=je:(n=We,o=void 0);const c=n===We&&t[e+1].startsWith("/>")?" ":"";r+=n===je?i+Ne:d>=0?(s.push(l),i.slice(0,d)+"$lit$"+i.slice(d)+Le+c):i+Le+(-2===d?(s.push(void 0),e):c)}const l=r+(t[i]||"<?>")+(2===e?"</svg>":"");if(!Array.isArray(t)||!t.hasOwnProperty("raw"))throw Error("invalid template strings array");return[void 0!==Re?Re.createHTML(l):l,s]})(t,e);if(this.el=Ye.createElement(a,i),Xe.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(s=Xe.nextNode())&&l.length<n;){if(1===s.nodeType){if(s.hasAttributes()){const t=[];for(const e of s.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith(Le)){const i=d[r++];if(t.push(e),void 0!==i){const t=s.getAttribute(i.toLowerCase()+"$lit$").split(Le),e=/([.?@])?(.*)/.exec(i);l.push({type:1,index:o,name:e[2],strings:t,ctor:"."===e[1]?oi:"?"===e[1]?ni:"@"===e[1]?li:si})}else l.push({type:6,index:o})}for(const e of t)s.removeAttribute(e)}if(Je.test(s.tagName)){const t=s.textContent.split(Le),e=t.length-1;if(e>0){s.textContent=He?He.emptyScript:"";for(let i=0;i<e;i++)s.append(t[i],ze()),Xe.nextNode(),l.push({type:2,index:++o});s.append(t[e],ze())}}}else if(8===s.nodeType)if(s.data===Me)l.push({type:2,index:o});else{let t=-1;for(;-1!==(t=s.data.indexOf(Le,t+1));)l.push({type:7,index:o}),t+=Le.length-1}o++}}static createElement(t,e){const i=Oe.createElement("template");return i.innerHTML=t,i}}function ti(t,e,i=t,s){var o,r,n,l;if(e===Ge)return e;let a=void 0!==s?null===(o=i._$Cl)||void 0===o?void 0:o[s]:i._$Cu;const d=De(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==d&&(null===(r=null==a?void 0:a._$AO)||void 0===r||r.call(a,!1),void 0===d?a=void 0:(a=new d(t),a._$AT(t,i,s)),void 0!==s?(null!==(n=(l=i)._$Cl)&&void 0!==n?n:l._$Cl=[])[s]=a:i._$Cu=a),void 0!==a&&(e=ti(t,a._$AS(t,e.values),a,s)),e}class ei{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:i},parts:s}=this._$AD,o=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:Oe).importNode(i,!0);Xe.currentNode=o;let r=Xe.nextNode(),n=0,l=0,a=s[0];for(;void 0!==a;){if(n===a.index){let e;2===a.type?e=new ii(r,r.nextSibling,this,t):1===a.type?e=new a.ctor(r,a.name,a.strings,this,t):6===a.type&&(e=new ai(r,this,t)),this.v.push(e),a=s[++l]}n!==(null==a?void 0:a.index)&&(r=Xe.nextNode(),n++)}return o}m(t){let e=0;for(const i of this.v)void 0!==i&&(void 0!==i.strings?(i._$AI(t,i,e),e+=i.strings.length-2):i._$AI(t[e])),e++}}class ii{constructor(t,e,i,s){var o;this.type=2,this._$AH=Ze,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=i,this.options=s,this._$Cg=null===(o=null==s?void 0:s.isConnected)||void 0===o||o}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=ti(this,t,e),De(t)?t===Ze||null==t||""===t?(this._$AH!==Ze&&this._$AR(),this._$AH=Ze):t!==this._$AH&&t!==Ge&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.k(t):(t=>{var e;return Be(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])})(t)?this.S(t):this.$(t)}M(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}k(t){this._$AH!==t&&(this._$AR(),this._$AH=this.M(t))}$(t){this._$AH!==Ze&&De(this._$AH)?this._$AA.nextSibling.data=t:this.k(Oe.createTextNode(t)),this._$AH=t}T(t){var e;const{values:i,_$litType$:s}=t,o="number"==typeof s?this._$AC(t):(void 0===s.el&&(s.el=Ye.createElement(s.h,this.options)),s);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===o)this._$AH.m(i);else{const t=new ei(o,this),e=t.p(this.options);t.m(i),this.k(e),this._$AH=t}}_$AC(t){let e=Qe.get(t.strings);return void 0===e&&Qe.set(t.strings,e=new Ye(t)),e}S(t){Be(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let i,s=0;for(const o of t)s===e.length?e.push(i=new ii(this.M(ze()),this.M(ze()),this,this.options)):i=e[s],i._$AI(o),s++;s<e.length&&(this._$AR(i&&i._$AB.nextSibling,s),e.length=s)}_$AR(t=this._$AA.nextSibling,e){var i;for(null===(i=this._$AP)||void 0===i||i.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class si{constructor(t,e,i,s,o){this.type=1,this._$AH=Ze,this._$AN=void 0,this.element=t,this.name=e,this._$AM=s,this.options=o,i.length>2||""!==i[0]||""!==i[1]?(this._$AH=Array(i.length-1).fill(new String),this.strings=i):this._$AH=Ze}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,i,s){const o=this.strings;let r=!1;if(void 0===o)t=ti(this,t,e,0),r=!De(t)||t!==this._$AH&&t!==Ge,r&&(this._$AH=t);else{const s=t;let n,l;for(t=o[0],n=0;n<o.length-1;n++)l=ti(this,s[i+n],e,n),l===Ge&&(l=this._$AH[n]),r||(r=!De(l)||l!==this._$AH[n]),l===Ze?t=Ze:t!==Ze&&(t+=(null!=l?l:"")+o[n+1]),this._$AH[n]=l}r&&!s&&this.C(t)}C(t){t===Ze?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class oi extends si{constructor(){super(...arguments),this.type=3}C(t){this.element[this.name]=t===Ze?void 0:t}}const ri=He?He.emptyScript:"";class ni extends si{constructor(){super(...arguments),this.type=4}C(t){t&&t!==Ze?this.element.setAttribute(this.name,ri):this.element.removeAttribute(this.name)}}class li extends si{constructor(t,e,i,s,o){super(t,e,i,s,o),this.type=5}_$AI(t,e=this){var i;if((t=null!==(i=ti(this,t,e,0))&&void 0!==i?i:Ze)===Ge)return;const s=this._$AH,o=t===Ze&&s!==Ze||t.capture!==s.capture||t.once!==s.once||t.passive!==s.passive,r=t!==Ze&&(s===Ze||o);o&&this.element.removeEventListener(this.name,this,s),r&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,i;"function"==typeof this._$AH?this._$AH.call(null!==(i=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==i?i:this.element,t):this._$AH.handleEvent(t)}}class ai{constructor(t,e,i){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=i}get _$AU(){return this._$AM._$AU}_$AI(t){ti(this,t)}}const di=window.litHtmlPolyfillSupport;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var hi,ci;null==di||di(Ye,ii),(null!==(ke=globalThis.litHtmlVersions)&&void 0!==ke?ke:globalThis.litHtmlVersions=[]).push("2.2.6");class pi extends Ue{constructor(){super(...arguments),this.renderOptions={host:this},this._$Do=void 0}createRenderRoot(){var t,e;const i=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=i.firstChild),i}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Do=((t,e,i)=>{var s,o;const r=null!==(s=null==i?void 0:i.renderBefore)&&void 0!==s?s:e;let n=r._$litPart$;if(void 0===n){const t=null!==(o=null==i?void 0:i.renderBefore)&&void 0!==o?o:null;r._$litPart$=n=new ii(e.insertBefore(ze(),t),t,void 0,null!=i?i:{})}return n._$AI(t),n})(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Do)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Do)||void 0===t||t.setConnected(!1)}render(){return Ge}}pi.finalized=!0,pi._$litElement$=!0,null===(hi=globalThis.litElementHydrateSupport)||void 0===hi||hi.call(globalThis,{LitElement:pi});const ui=globalThis.litElementPolyfillSupport;null==ui||ui({LitElement:pi}),(null!==(ci=globalThis.litElementVersions)&&void 0!==ci?ci:globalThis.litElementVersions=[]).push("3.2.1");
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const gi=(t,e)=>"method"===e.kind&&e.descriptor&&!("value"in e.descriptor)?{...e,finisher(i){i.createProperty(e.key,t)}}:{kind:"field",key:Symbol(),placement:"own",descriptor:{},originalKey:e.key,initializer(){"function"==typeof e.initializer&&(this[e.key]=e.initializer.call(this))},finisher(i){i.createProperty(e.key,t)}};function vi(t){return(e,i)=>void 0!==i?((t,e,i)=>{e.constructor.createProperty(i,t)})(t,e,i):gi(t,e)
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}var fi;null===(fi=window.HTMLSlotElement)||void 0===fi||fi.prototype.assignedElements;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const $i=2;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
class mi extends class{constructor(t){}get _$AU(){return this._$AM._$AU}_$AT(t,e,i){this._$Ct=t,this._$AM=e,this._$Ci=i}_$AS(t,e){return this.update(t,e)}update(t,e){return this.render(...e)}}{constructor(t){if(super(t),this.it=Ze,t.type!==$i)throw Error(this.constructor.directiveName+"() can only be used in child bindings")}render(t){if(t===Ze||null==t)return this.ft=void 0,this.it=t;if(t===Ge)return t;if("string"!=typeof t)throw Error(this.constructor.directiveName+"() called with a non-string value");if(t===this.it)return this.ft;this.it=t;const e=[t];return e.raw=e,this.ft={_$litType$:this.constructor.resultType,strings:e,values:[]}}}mi.directiveName="unsafeHTML",mi.resultType=1;const yi=(t=>(...e)=>({_$litDirective$:t,values:e}))(mi),_i="",Ai="dummy-title",bi="",wi="",Si="",Ei="",xi="",Ci="",Ti="",Pi=!1,Ui=1,ki="";class Hi extends pi{constructor(){super(...arguments),this.mainTitle=Ai,this.subTitle=bi,this.author=_i,this.date=wi,this.url=Si,this.imgUrl=xi,this.imgSrc=Ei,this.organisation=Ci,this.organisationUrl=Ti,this.jsonConfig=ki,this.full=Pi,this.fullScaleFactor=Ui,this.goToPrint=!1,this._updateView=(t=!1)=>{if(this.goToPrint)return;t&&(this.full=!this.full);const e=this.shadowRoot.querySelector(".dd-caption");e&&(this.full?e.style.display="none":e.style.display="flex");if(document.querySelector(".shower"))return void(this.full?this.classList.remove("list"):this.classList.add("list"));if(this.full){this.classList.add("full"),this.classList.remove("list");const{innerWidth:t,innerHeight:e}=window,i=document.querySelector(".slide");if(i){const{offsetWidth:s,offsetHeight:o}=i,r=1/Math.max(s/t,o/e)*this.fullScaleFactor;this.style.setProperty("--slide-collect-full-scale-factor",`${r}`);const n=(e-o*r)/2,l=document.querySelectorAll(".slide");this.style.height=l.length*e+"px";for(const[i,o]of l.entries())o.id=`${i+1}`,o.style.marginTop=`\n              ${e*i+n}px`,o.style.marginLeft=`\n              ${(t-s*r)/2}px`,i===l.length-1&&(o.style.marginBottom=`${n}px`)}}else{this.classList.add("list"),this.classList.remove("full"),this.style.height="auto";const t=document.querySelectorAll(".slide");for(const e of t)e.style.marginTop="0px",e.style.marginLeft="0px"}const i=`${window.location.protocol}//${window.location.host}${window.location.pathname}`;this.full?window.history.pushState({fullPage:!0},"Slide",`${i}?full`):window.history.pushState({fullPage:!0},"Slide",`${i}`),this._updateStyle()},this._handleSlideClick=()=>{const t=document.querySelectorAll(".slide");for(const[e,i]of t.entries())i.addEventListener("click",(()=>{if(!this.full){if(this._updateView(!0),document.querySelector(".shower"))return;window.scrollBy(0,window.innerHeight*e),window.location.hash=`#${e+1}`}}))},this._handleResize=()=>{document.querySelector(".shower")||this._updateView()},this._handleLocation=()=>{const t=window.location.href.split("?")[1],e=new URLSearchParams(t);for(const t of e.entries())if(t[0].includes("full")){this.full||this._updateView(!0);break}},this._handleScroll=()=>{if(!document.querySelector(".shower")&&!this.goToPrint&&this.full){const t=document.documentElement.scrollTop,e=Math.floor(t/window.innerHeight);window.history.pushState(null,"",`?full#${e+1}`)}},this._interactKeys=()=>{document.onkeydown=t=>{const e=t||window.event;if(!(e.ctrlKey||e.altKey||e.metaKey))switch(e.key.toUpperCase()){case"ESC":case"ESCAPE":this.full&&(e.preventDefault(),this._updateView(!0));break;case"BACKSPACE":case"PAGEUP":case"ARROWUP":case"ARROWLEFT":case"P":this.full&&(e.preventDefault(),window.scrollBy(0,-window.innerHeight));break;case"PAGEDOWN":case"ARROWDOWN":case"ARROWRIGHT":case"N":this.full&&(e.preventDefault(),window.scrollBy(0,window.innerHeight));break;case" ":this.full&&(e.preventDefault(),e.shiftKey?window.scrollBy(0,-window.innerHeight):window.scrollBy(0,window.innerHeight))}}},this._updateStyle=()=>{this.full?this.parentElement.style.backgroundColor="rgba(0,0,0,1)":this.parentElement.style.backgroundColor=""}}async setPropsFromJson(){const t=await async function(t){if(await(async t=>404!==(await fetch(t,{method:"HEAD"})).status||(console.error(`JSON config does not exist at '${t}'`),!1))(t))try{const e=await fetch(t);return await e.json()}catch(e){console.error(`Error while reading config file at ${t} \n\n ${e}`)}return{error:"Could not parse JSON config, see console for errors"}}(this.jsonConfig);t.error?this.mainTitle=`<i><b>[ERROR]</b>${t.error} </i>`:(t.title&&(this.mainTitle=t.title),t.mainTitle&&(this.mainTitle=t.mainTitle),t.subTitle&&(this.subTitle=t.subTitle),t.author&&this.author===_i&&(this.author=t.author),t.date&&(this.date=t.date),t.url&&(this.url=t.url),t.imgUrl&&(this.imgUrl=t.imgUrl),t.imgSrc&&(this.imgSrc=t.imgSrc),t.organisation&&(this.organisation=t.organisation),t.organisationUrl&&(this.organisationUrl=t.organisationUrl))}makeCaptionHeader(t=""){let e="";this.imgSrc&&(e=`<a class="caption-link"\n          href="${this.imgUrl}"\n          target="_blank"\n          title="Click to see IMG link">\n          <img class="caption-img" src="${this.imgSrc}" alt="img-src"></a>`);let i=`${this.organisation}`;this.organisationUrl&&(i=`<a href="${this.organisationUrl}"\n                           title="Organisation (click for link)"\n                           class="dd-slide-collect-org-url">${this.organisation}</a>`);let s="";return this.url&&(s=`\n      <a class="caption-url" href="${this.url}" target="_blank">\n          <i>${this.url}</i>\n      </a>`),t?`\n        <div class="dd-caption-custom">\n          ${t}\n        </div>\n        `:`\n        <header class="dd-caption" title="Slide collection caption">\n          <div class="dd-caption-item dd-caption-left">\n            ${e}\n          </div>\n          <div class="dd-caption-item dd-caption-center">\n            <div class="dd-caption-title" title="Title">\n              ${this.mainTitle}<br>\n            </div>\n            <div class="dd-caption-subtitle" title="Subtitle">\n              <i>${this.subTitle}</i>\n            </div>\n          </div>\n          <div class="dd-caption-item dd-caption-right">\n            <span title="Date">${this.date}</span><br>\n            <strong title="Author(s)">${this.author}</strong><br>\n            <span title="Organisation">${i}</span><br>\n            ${s}\n          </div>\n        </header>\n      `}connectedCallback(){super.connectedCallback(),window.addEventListener("DOMContentLoaded",this._handleSlideClick),window.addEventListener("DOMContentLoaded",this._handleLocation),window.addEventListener("resize",this._handleResize),document.addEventListener("scroll",this._handleScroll),window.addEventListener("beforeprint",(()=>{this.goToPrint=!0})),window.addEventListener("afterprint",(()=>{this.goToPrint=!1}))}disconnectedCallback(){window.removeEventListener("DOMContentLoaded",this._handleSlideClick),window.removeEventListener("DOMContentLoaded",this._handleLocation),window.removeEventListener("resize",this._handleResize),document.removeEventListener("scroll",this._handleScroll),window.removeEventListener("beforeprint",(()=>{this.goToPrint=!0})),window.removeEventListener("afterprint",(()=>{this.goToPrint=!1})),super.disconnectedCallback()}async firstUpdated(){this.full?this.classList.add("full"):this.classList.add("list");document.querySelector(".shower")||(this.parentElement.style.margin="0"),this._interactKeys(),this._updateStyle()}render(){let t="";this.jsonConfig&&this.setPropsFromJson();const e=this.querySelector('[slot="caption"]');e?(t+=this.makeCaptionHeader(e.innerHTML),e.style.display="none"):t+=this.makeCaptionHeader(),t+='<slot name="caption"></slot>',t+="<slot></slot>";const i=this.querySelectorAll('dd-slide-collection > *:not(.slide):not([slot="caption"]):not(section)');for(const t of i)t.innerHTML=`\n<i>[WARNING] <code>${t.nodeName}</code> element is not allowed.\n<br>\n<br>\nOnly include <code>section and dd-slide</code> elements, or elements with\n<code>.slide</code> class inside a <code>dd-slide-collection</code>, otherwise,\nthe slide will not be rendered properly</i>\n`;return Ke`${yi(t)}`}}function Ri(t,e,i,s){var o,r=arguments.length,n=r<3?e:null===s?s=Object.getOwnPropertyDescriptor(e,i):s;if("object"==typeof Reflect&&"function"==typeof Reflect.decorate)n=Reflect.decorate(t,e,i,s);else for(var l=t.length-1;l>=0;l--)(o=t[l])&&(n=(r<3?o(n):r>3?o(e,i,n):o(e,i))||n);return r>3&&n&&Object.defineProperty(e,i,n),n
/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}Hi.styles=((t,...e)=>{const i=1===t.length?t[0]:e.reduce(((e,i,s)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(i)+t[s+1]),t[0]);return new Ae(i,t,ye)})`
    :host {
      /* Ddpres fillers */

      /* dd color pallette */
      --slide-collect-color-prim: var(--dd-color-prim, rgba(153, 155, 132, 1));
      --slide-collect-color-prim-dark: var(
        --dd-color-prim-dark,
        rgba(65, 90, 72, 1)
      );
      --slide-collect-color-sec: var(--dd-color-sec, rgba(248, 237, 227, 1));
      --slide-collect-list-bg-color: var(
        --dd-color-list-bg,
        rgba(248, 237, 227, 0.5)
      );
      --slide-collect-text-color: var(--dd-color-text, rgba(0, 0, 0, 0.9));
      --slide-collect-text-color-light: var(
        --dd-color-text-light,
        rgba(255, 255, 255, 1)
      );

      --slide-collect-gap: var(--dd-slide-gap, 96px);
      --slide-collect-ratio: var(--dd-slide-ratio, calc(16 / 9));
      --slide-collect-width: var(--dd-slide-width, 1024px);
      --slide-collect-height: var(
        --dd-slide-height,
        calc(var(--slide-collect-width) / var(--slide-collect-ratio))
      );
      --slide-collect-full-scale-factor: var(--dd-full-scale-factor, 1);
      --slide-collect-font: var(--dd-font, 24px/2 'Roboto', sans-serif);
      --slide-collect-font-size: var(--dd-font-size, 24px);

      --caption-height: var(--dd-caption-height, 250px);
      --caption-center-width: 60%;
      --caption-font-size: calc(1.8 * var(--slide-collect-font-size));
      --caption-padding-left: 30px;
      --caption-padding-top: 30px;
      --caption-padding-right: 30px;
      --caption-padding-bottom: 30px;
      --caption-img-height: calc(0.6 * var(--caption-height));
      --caption-fg-color: var(
        --dd-color-caption-fg,
        var(--slide-collect-text-color-light)
      );
      --caption-bg-color: var(
        --dd-color-caption-bg,
        var(--slide-collect-color-prim-dark)
      );

      --slide-collect-slide-nr-font-size: var(--dd-slide-nr-font-size, 16px);
      --slide-collect-slide-nr-right: var(--dd-slide-nr-right, 13px);
      --slide-collect-slide-nr-bottom: calc(
        var(--dd-slide-nr-bottom, 0.2em) +
          var(--dd-footer-bottom, var(--progress-size, 0em))
      );
      --slide-collect-slide-nr-color: var(
        --dd-slide-nr-color,
        var(--slide-collect-text-color)
      );

      --slide-scale: 1;

      margin: 0;
      padding: 0;
      color: black;
      counter-reset: slide;

      font: var(--slide-collect-font);
      font-size: var(--slide-collect-font-size);
    }

    /* Full */

    :host(.full) {
      display: block;
    }

    /* Hover */

    :host(.list) ::slotted(section:hover),
    :host(.list) ::slotted(.slide:hover) {
      box-shadow: 0 0 0 20px var(--slide-collect-color-prim);
    }

    ::slotted(section),
    ::slotted(.slide) {
      position: relative;
      z-index: 0;
      overflow: hidden;
      box-sizing: border-box;
      width: var(--slide-collect-width);
      max-width: 100%;
      height: var(--slide-collect-height);
      background-color: white;
      margin: 0 auto;
      /*margin-bottom: var(--slide-collect-gap)*/
    }

    :host(.full) ::slotted(section),
    :host(.full) ::slotted(.slide) {
      position: absolute;
      transform-origin: 0 0;
      transform: scale(var(--slide-collect-full-scale-factor));
      border: 1px solid black;
    }

    /* Number */

    ::slotted(section)::after,
    ::slotted(.slide)::after {
      position: absolute;
      font-size: var(--slide-collect-slide-nr-font-size);
      right: var(--slide-collect-slide-nr-right);
      bottom: calc(var(--slide-collect-slide-nr-bottom));
      left: 0px;
      color: var(--slide-collect-slide-nr-color);
      text-align: right;
      counter-increment: slide;
      content: counter(slide);
      z-index: 2;
    }

    /* List */

    :host(.list) ::slotted(.slide),
    :host(.list) ::slotted(section) {
      position: relative;
      box-shadow: calc(var(--slide-scale) * 4px) calc(var(--slide-scale) * 4px)
        0 calc(var(--slide-scale) * 4px) var(--slide-collect-color-prim-dark);
      transform-origin: 0 0;
      transform: scale(var(--slide-scale));
      display: block;
      min-width: var(--slide-collect-width);
    }

    :host(.list) ::slotted(section *),
    :host(.list) ::slotted(.slide *) {
      pointer-events: none;
    }

    :host(.list) {
      padding: calc(var(--slide-collect-gap) * var(--slide-scale));
      box-sizing: border-box;
      width: 100%;
      display: grid;
      grid-gap: calc(var(--slide-collect-gap) * var(--slide-scale));
      grid-auto-rows: calc(var(--slide-collect-height) * var(--slide-scale));
      grid-template-rows: min-content;
      grid-template-columns: repeat(
        auto-fill,
        calc(var(--slide-collect-width) * var(--slide-scale))
      );
      background-color: var(--slide-collect-color-sec-alpha);
      /*overflow-x: hidden;*/
    }

    :host(.list) .dd-caption,
    :host(.list) .dd-caption-custom {
      grid-column: 1 / -1;
      margin-top: calc(-1 * var(--slide-collect-gap) * var(--slide-scale));
      margin-left: calc(-1 * var(--slide-collect-gap) * var(--slide-scale));
      box-sizing: border-box;
      width: 100vw;
    }

    /* IE & Edge Fix */

    :host(.list):not(.dd-caption):not(.dd-caption-custom) {
      position: absolute;
      clip: rect(0, auto, auto, 0);
    }

    /* Responsive */

    :host(.list) {
      --slide-scale: 0.25;
    }

    @media (min-width: 1168px) {
      :host(.list) {
        --slide-scale: 0.5;
      }
    }

    @media (min-width: 2336px) {
      :host(.list) {
        --slide-scale: 1;
      }
    }

    @media (max-width: 1168px) {
      :host {
        --caption-font-size: calc(1.3 * var(--slide-collect-font-size));
      }
    }
    @media (max-width: 700px) {
      :host {
        --caption-font-size: calc(1.1 * var(--slide-collect-font-size));
      }
    }

    /* Caption */

    .dd-caption {
      width: 100%;
      position: relative;
      display: flex;
      flex-direction: row;
      z-index: 1;
      color: var(--caption-fg-color);
      background-color: var(--caption-bg-color);
      height: var(--caption-height);
    }

    .dd-caption-item {
      font-size: var(--caption-font-size);
    }

    .dd-caption-left {
      flex-grow: 1;
      text-align: left;
      align-self: center;
      padding: var(--caption-padding-top) 0 var(--caption-padding-bottom)
        var(--caption-padding-left);
    }

    .dd-caption-center {
      flex-grow: 6;
      text-align: left;
      max-width: var(--caption-center-width);
      padding: var(--caption-padding-top) var(--caption-padding-right)
        var(--caption-padding-bottom) var(--caption-padding-left);
    }

    .dd-caption-title {
      padding-top: 20px;
      line-height: 1em;
      font-size: var(--caption-font-size);
      font-weight: 300;
    }

    .dd-caption-subtitle {
      padding-top: 20px;
      line-height: 1em;
      font-size: calc(0.7 * var(--caption-font-size));
      color: var(--caption-color-subtitle, rgba(255, 255, 255, 0.7));
    }

    .dd-caption-right {
      flex-grow: 1;
      text-align: right;
      font-size: calc(0.45 * var(--caption-font-size));
      align-self: flex-end;
      padding: var(--caption-padding-top) var(--caption-padding-right)
        var(--caption-padding-bottom) 0;
    }

    img.caption-img {
      height: var(--caption-img-height);
      display: block;
    }

    .caption-url {
      /*text-decoration: none;*/
      color: var(--caption-fg-color);
    }

    .dd-slide-collect-org-url {
      color: var(--slide-collect-text-color-light);
    }

    /* do not show custom caption slot */
    .dd-caption-custom {
      width: 100%;
      z-index: 1;
    }

    /* Print */

    @media print {
      :host(.full) {
        display: inline;
      }

      :host(.full) ::slotted(section),
      :host(.full) ::slotted(.slide) {
        position: relative;
        margin-left: 0 !important;
        margin-top: 0 !important;
        margin-bottom: 0 !important;
        transform: none;
      }
    }
  `,$e([vi({type:String,attribute:"main-title"})],Hi.prototype,"mainTitle",void 0),$e([vi({type:String,attribute:"sub-title"})],Hi.prototype,"subTitle",void 0),$e([vi({type:String,attribute:"author"})],Hi.prototype,"author",void 0),$e([vi({type:String,attribute:"date"})],Hi.prototype,"date",void 0),$e([vi({type:String,attribute:"url"})],Hi.prototype,"url",void 0),$e([vi({type:String,attribute:"img-url"})],Hi.prototype,"imgUrl",void 0),$e([vi({type:String,attribute:"img-src"})],Hi.prototype,"imgSrc",void 0),$e([vi({type:String,attribute:"organisation"})],Hi.prototype,"organisation",void 0),$e([vi({type:String,attribute:"organisation-url"})],Hi.prototype,"organisationUrl",void 0),$e([vi({type:String,attribute:"config-path"})],Hi.prototype,"jsonConfig",void 0),$e([vi({type:Boolean,attribute:"full"})],Hi.prototype,"full",void 0),$e([vi({type:Number,attribute:"full-scale-factor"})],Hi.prototype,"fullScaleFactor",void 0),$e([vi({type:Boolean,attribute:!1})],Hi.prototype,"goToPrint",void 0),window.customElements.define("dd-slide-collection",Hi);const Li=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,Mi=Symbol(),Ni=new Map;class Oi{constructor(t,e){if(this._$cssResult$=!0,e!==Mi)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){let t=Ni.get(this.cssText);return Li&&void 0===t&&(Ni.set(this.cssText,t=new CSSStyleSheet),t.replaceSync(this.cssText)),t}toString(){return this.cssText}}const zi=Li?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const i of t.cssRules)e+=i.cssText;return(t=>new Oi("string"==typeof t?t:t+"",Mi))(e)})(t):t
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */;var Di;const Bi=window.trustedTypes,ji=Bi?Bi.emptyScript:"",Ii=window.reactiveElementPolyfillSupport,Vi={toAttribute(t,e){switch(e){case Boolean:t=t?ji:null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let i=t;switch(e){case Boolean:i=null!==t;break;case Number:i=null===t?null:Number(t);break;case Object:case Array:try{i=JSON.parse(t)}catch(t){i=null}}return i}},Wi=(t,e)=>e!==t&&(e==e||t==t),qi={attribute:!0,type:String,converter:Vi,reflect:!1,hasChanged:Wi};class Fi extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(t){var e;null!==(e=this.l)&&void 0!==e||(this.l=[]),this.l.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,i)=>{const s=this._$Eh(i,e);void 0!==s&&(this._$Eu.set(s,i),t.push(s))})),t}static createProperty(t,e=qi){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const i="symbol"==typeof t?Symbol():"__"+t,s=this.getPropertyDescriptor(t,i,e);void 0!==s&&Object.defineProperty(this.prototype,t,s)}}static getPropertyDescriptor(t,e,i){return{get(){return this[e]},set(s){const o=this[t];this[e]=s,this.requestUpdate(t,o,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||qi}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const i of e)this.createProperty(i,t[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const i=new Set(t.flat(1/0).reverse());for(const t of i)e.unshift(zi(t))}else void 0!==t&&e.push(zi(t));return e}static _$Eh(t,e){const i=e.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof t?t.toLowerCase():void 0}o(){var t;this._$Ep=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Em(),this.requestUpdate(),null===(t=this.constructor.l)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,i;(null!==(e=this._$Eg)&&void 0!==e?e:this._$Eg=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(i=t.hostConnected)||void 0===i||i.call(t))}removeController(t){var e;null===(e=this._$Eg)||void 0===e||e.splice(this._$Eg.indexOf(t)>>>0,1)}_$Em(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Et.set(e,this[e]),delete this[e])}))}createRenderRoot(){var t;const e=null!==(t=this.shadowRoot)&&void 0!==t?t:this.attachShadow(this.constructor.shadowRootOptions);return((t,e)=>{Li?t.adoptedStyleSheets=e.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):e.forEach((e=>{const i=document.createElement("style"),s=window.litNonce;void 0!==s&&i.setAttribute("nonce",s),i.textContent=e.cssText,t.appendChild(i)}))})(e,this.constructor.elementStyles),e}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,i){this._$AK(t,i)}_$ES(t,e,i=qi){var s,o;const r=this.constructor._$Eh(t,i);if(void 0!==r&&!0===i.reflect){const n=(null!==(o=null===(s=i.converter)||void 0===s?void 0:s.toAttribute)&&void 0!==o?o:Vi.toAttribute)(e,i.type);this._$Ei=t,null==n?this.removeAttribute(r):this.setAttribute(r,n),this._$Ei=null}}_$AK(t,e){var i,s,o;const r=this.constructor,n=r._$Eu.get(t);if(void 0!==n&&this._$Ei!==n){const t=r.getPropertyOptions(n),l=t.converter,a=null!==(o=null!==(s=null===(i=l)||void 0===i?void 0:i.fromAttribute)&&void 0!==s?s:"function"==typeof l?l:null)&&void 0!==o?o:Vi.fromAttribute;this._$Ei=n,this[n]=a(e,t.type),this._$Ei=null}}requestUpdate(t,e,i){let s=!0;void 0!==t&&(((i=i||this.constructor.getPropertyOptions(t)).hasChanged||Wi)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===i.reflect&&this._$Ei!==t&&(void 0===this._$EC&&(this._$EC=new Map),this._$EC.set(t,i))):s=!1),!this.isUpdatePending&&s&&(this._$Ep=this._$E_())}async _$E_(){this.isUpdatePending=!0;try{await this._$Ep}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((t,e)=>this[e]=t)),this._$Et=void 0);let e=!1;const i=this._$AL;try{e=this.shouldUpdate(i),e?(this.willUpdate(i),null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(i)):this._$EU()}catch(t){throw e=!1,this._$EU(),t}e&&this._$AE(i)}willUpdate(t){}_$AE(t){var e;null===(e=this._$Eg)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$EU(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ep}shouldUpdate(t){return!0}update(t){void 0!==this._$EC&&(this._$EC.forEach(((t,e)=>this._$ES(e,this[e],t))),this._$EC=void 0),this._$EU()}updated(t){}firstUpdated(t){}}
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var Ji;Fi.finalized=!0,Fi.elementProperties=new Map,Fi.elementStyles=[],Fi.shadowRootOptions={mode:"open"},null==Ii||Ii({ReactiveElement:Fi}),(null!==(Di=globalThis.reactiveElementVersions)&&void 0!==Di?Di:globalThis.reactiveElementVersions=[]).push("1.3.2");const Ki=globalThis.trustedTypes,Gi=Ki?Ki.createPolicy("lit-html",{createHTML:t=>t}):void 0,Zi=`lit$${(Math.random()+"").slice(9)}$`,Qi="?"+Zi,Xi=`<${Qi}>`,Yi=document,ts=(t="")=>Yi.createComment(t),es=t=>null===t||"object"!=typeof t&&"function"!=typeof t,is=Array.isArray,ss=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,os=/-->/g,rs=/>/g,ns=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,ls=/'/g,as=/"/g,ds=/^(?:script|style|textarea|title)$/i,hs=(t=>(e,...i)=>({_$litType$:t,strings:e,values:i}))(1),cs=Symbol.for("lit-noChange"),ps=Symbol.for("lit-nothing"),us=new WeakMap,gs=Yi.createTreeWalker(Yi,129,null,!1);class vs{constructor({strings:t,_$litType$:e},i){let s;this.parts=[];let o=0,r=0;const n=t.length-1,l=this.parts,[a,d]=((t,e)=>{const i=t.length-1,s=[];let o,r=2===e?"<svg>":"",n=ss;for(let e=0;e<i;e++){const i=t[e];let l,a,d=-1,h=0;for(;h<i.length&&(n.lastIndex=h,a=n.exec(i),null!==a);)h=n.lastIndex,n===ss?"!--"===a[1]?n=os:void 0!==a[1]?n=rs:void 0!==a[2]?(ds.test(a[2])&&(o=RegExp("</"+a[2],"g")),n=ns):void 0!==a[3]&&(n=ns):n===ns?">"===a[0]?(n=null!=o?o:ss,d=-1):void 0===a[1]?d=-2:(d=n.lastIndex-a[2].length,l=a[1],n=void 0===a[3]?ns:'"'===a[3]?as:ls):n===as||n===ls?n=ns:n===os||n===rs?n=ss:(n=ns,o=void 0);const c=n===ns&&t[e+1].startsWith("/>")?" ":"";r+=n===ss?i+Xi:d>=0?(s.push(l),i.slice(0,d)+"$lit$"+i.slice(d)+Zi+c):i+Zi+(-2===d?(s.push(void 0),e):c)}const l=r+(t[i]||"<?>")+(2===e?"</svg>":"");if(!Array.isArray(t)||!t.hasOwnProperty("raw"))throw Error("invalid template strings array");return[void 0!==Gi?Gi.createHTML(l):l,s]})(t,e);if(this.el=vs.createElement(a,i),gs.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(s=gs.nextNode())&&l.length<n;){if(1===s.nodeType){if(s.hasAttributes()){const t=[];for(const e of s.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith(Zi)){const i=d[r++];if(t.push(e),void 0!==i){const t=s.getAttribute(i.toLowerCase()+"$lit$").split(Zi),e=/([.?@])?(.*)/.exec(i);l.push({type:1,index:o,name:e[2],strings:t,ctor:"."===e[1]?_s:"?"===e[1]?bs:"@"===e[1]?ws:ys})}else l.push({type:6,index:o})}for(const e of t)s.removeAttribute(e)}if(ds.test(s.tagName)){const t=s.textContent.split(Zi),e=t.length-1;if(e>0){s.textContent=Ki?Ki.emptyScript:"";for(let i=0;i<e;i++)s.append(t[i],ts()),gs.nextNode(),l.push({type:2,index:++o});s.append(t[e],ts())}}}else if(8===s.nodeType)if(s.data===Qi)l.push({type:2,index:o});else{let t=-1;for(;-1!==(t=s.data.indexOf(Zi,t+1));)l.push({type:7,index:o}),t+=Zi.length-1}o++}}static createElement(t,e){const i=Yi.createElement("template");return i.innerHTML=t,i}}function fs(t,e,i=t,s){var o,r,n,l;if(e===cs)return e;let a=void 0!==s?null===(o=i._$Cl)||void 0===o?void 0:o[s]:i._$Cu;const d=es(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==d&&(null===(r=null==a?void 0:a._$AO)||void 0===r||r.call(a,!1),void 0===d?a=void 0:(a=new d(t),a._$AT(t,i,s)),void 0!==s?(null!==(n=(l=i)._$Cl)&&void 0!==n?n:l._$Cl=[])[s]=a:i._$Cu=a),void 0!==a&&(e=fs(t,a._$AS(t,e.values),a,s)),e}class $s{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:i},parts:s}=this._$AD,o=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:Yi).importNode(i,!0);gs.currentNode=o;let r=gs.nextNode(),n=0,l=0,a=s[0];for(;void 0!==a;){if(n===a.index){let e;2===a.type?e=new ms(r,r.nextSibling,this,t):1===a.type?e=new a.ctor(r,a.name,a.strings,this,t):6===a.type&&(e=new Ss(r,this,t)),this.v.push(e),a=s[++l]}n!==(null==a?void 0:a.index)&&(r=gs.nextNode(),n++)}return o}m(t){let e=0;for(const i of this.v)void 0!==i&&(void 0!==i.strings?(i._$AI(t,i,e),e+=i.strings.length-2):i._$AI(t[e])),e++}}class ms{constructor(t,e,i,s){var o;this.type=2,this._$AH=ps,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=i,this.options=s,this._$Cg=null===(o=null==s?void 0:s.isConnected)||void 0===o||o}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=fs(this,t,e),es(t)?t===ps||null==t||""===t?(this._$AH!==ps&&this._$AR(),this._$AH=ps):t!==this._$AH&&t!==cs&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.k(t):(t=>{var e;return is(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])})(t)?this.S(t):this.$(t)}M(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}k(t){this._$AH!==t&&(this._$AR(),this._$AH=this.M(t))}$(t){this._$AH!==ps&&es(this._$AH)?this._$AA.nextSibling.data=t:this.k(Yi.createTextNode(t)),this._$AH=t}T(t){var e;const{values:i,_$litType$:s}=t,o="number"==typeof s?this._$AC(t):(void 0===s.el&&(s.el=vs.createElement(s.h,this.options)),s);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===o)this._$AH.m(i);else{const t=new $s(o,this),e=t.p(this.options);t.m(i),this.k(e),this._$AH=t}}_$AC(t){let e=us.get(t.strings);return void 0===e&&us.set(t.strings,e=new vs(t)),e}S(t){is(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let i,s=0;for(const o of t)s===e.length?e.push(i=new ms(this.M(ts()),this.M(ts()),this,this.options)):i=e[s],i._$AI(o),s++;s<e.length&&(this._$AR(i&&i._$AB.nextSibling,s),e.length=s)}_$AR(t=this._$AA.nextSibling,e){var i;for(null===(i=this._$AP)||void 0===i||i.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class ys{constructor(t,e,i,s,o){this.type=1,this._$AH=ps,this._$AN=void 0,this.element=t,this.name=e,this._$AM=s,this.options=o,i.length>2||""!==i[0]||""!==i[1]?(this._$AH=Array(i.length-1).fill(new String),this.strings=i):this._$AH=ps}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,i,s){const o=this.strings;let r=!1;if(void 0===o)t=fs(this,t,e,0),r=!es(t)||t!==this._$AH&&t!==cs,r&&(this._$AH=t);else{const s=t;let n,l;for(t=o[0],n=0;n<o.length-1;n++)l=fs(this,s[i+n],e,n),l===cs&&(l=this._$AH[n]),r||(r=!es(l)||l!==this._$AH[n]),l===ps?t=ps:t!==ps&&(t+=(null!=l?l:"")+o[n+1]),this._$AH[n]=l}r&&!s&&this.C(t)}C(t){t===ps?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class _s extends ys{constructor(){super(...arguments),this.type=3}C(t){this.element[this.name]=t===ps?void 0:t}}const As=Ki?Ki.emptyScript:"";class bs extends ys{constructor(){super(...arguments),this.type=4}C(t){t&&t!==ps?this.element.setAttribute(this.name,As):this.element.removeAttribute(this.name)}}class ws extends ys{constructor(t,e,i,s,o){super(t,e,i,s,o),this.type=5}_$AI(t,e=this){var i;if((t=null!==(i=fs(this,t,e,0))&&void 0!==i?i:ps)===cs)return;const s=this._$AH,o=t===ps&&s!==ps||t.capture!==s.capture||t.once!==s.once||t.passive!==s.passive,r=t!==ps&&(s===ps||o);o&&this.element.removeEventListener(this.name,this,s),r&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,i;"function"==typeof this._$AH?this._$AH.call(null!==(i=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==i?i:this.element,t):this._$AH.handleEvent(t)}}class Ss{constructor(t,e,i){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=i}get _$AU(){return this._$AM._$AU}_$AI(t){fs(this,t)}}const Es=window.litHtmlPolyfillSupport;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var xs,Cs;null==Es||Es(vs,ms),(null!==(Ji=globalThis.litHtmlVersions)&&void 0!==Ji?Ji:globalThis.litHtmlVersions=[]).push("2.2.6");class Ts extends Fi{constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var t,e;const i=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=i.firstChild),i}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Dt=((t,e,i)=>{var s,o;const r=null!==(s=null==i?void 0:i.renderBefore)&&void 0!==s?s:e;let n=r._$litPart$;if(void 0===n){const t=null!==(o=null==i?void 0:i.renderBefore)&&void 0!==o?o:null;r._$litPart$=n=new ms(e.insertBefore(ts(),t),t,void 0,null!=i?i:{})}return n._$AI(t),n})(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!1)}render(){return cs}}Ts.finalized=!0,Ts._$litElement$=!0,null===(xs=globalThis.litElementHydrateSupport)||void 0===xs||xs.call(globalThis,{LitElement:Ts});const Ps=globalThis.litElementPolyfillSupport;null==Ps||Ps({LitElement:Ts}),(null!==(Cs=globalThis.litElementVersions)&&void 0!==Cs?Cs:globalThis.litElementVersions=[]).push("3.2.0");
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const Us=(t,e)=>"method"===e.kind&&e.descriptor&&!("value"in e.descriptor)?{...e,finisher(i){i.createProperty(e.key,t)}}:{kind:"field",key:Symbol(),placement:"own",descriptor:{},originalKey:e.key,initializer(){"function"==typeof e.initializer&&(this[e.key]=e.initializer.call(this))},finisher(i){i.createProperty(e.key,t)}};function ks(t){return(e,i)=>void 0!==i?((t,e,i)=>{e.constructor.createProperty(i,t)})(t,e,i):Us(t,e)
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}var Hs;null===(Hs=window.HTMLSlotElement)||void 0===Hs||Hs.prototype.assignedElements;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const Rs=2;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
class Ls extends class{constructor(t){}get _$AU(){return this._$AM._$AU}_$AT(t,e,i){this._$Ct=t,this._$AM=e,this._$Ci=i}_$AS(t,e){return this.update(t,e)}update(t,e){return this.render(...e)}}{constructor(t){if(super(t),this.it=ps,t.type!==Rs)throw Error(this.constructor.directiveName+"() can only be used in child bindings")}render(t){if(t===ps||null==t)return this.ft=void 0,this.it=t;if(t===cs)return t;if("string"!=typeof t)throw Error(this.constructor.directiveName+"() called with a non-string value");if(t===this.it)return this.ft;this.it=t;const e=[t];return e.raw=e,this.ft={_$litType$:this.constructor.resultType,strings:e,values:[]}}}Ls.directiveName="unsafeHTML",Ls.resultType=1;const Ms=(t=>(...e)=>({_$litDirective$:t,values:e}))(Ls),Ns="",Os="",zs="",Ds="",Bs="",js="",Is="",Vs="",Ws="",qs="",Fs="center",Js="dd-slide",Ks="dd-slide-collection";class Gs extends Ts{constructor(){super(...arguments),this.textLeft=Ns,this.imgLeft=Os,this.imgLeftLink=zs,this.textCenter=Ds,this.imgCenter=Bs,this.imgCenterLink=js,this.textRight=Is,this.imgRight=Vs,this.imgRightLink=Ws,this.configPath=qs,this.alignVertical=Fs,this.toSelector=Js,this.fromSelector=Ks,this._injectIntoSelector=()=>{const t=document.querySelectorAll(this.toSelector);if(t.length>0){this.style.display="none";for(const e of t){const t=document.createElement("dd-footer");t.setAttribute("text-left",this.textLeft),t.setAttribute("img-left",this.imgLeft),t.setAttribute("img-left-link",this.imgLeftLink),t.setAttribute("text-center",this.textCenter),t.setAttribute("img-center",this.imgCenter),t.setAttribute("img-center-link",this.imgCenterLink),t.setAttribute("text-right",this.textRight),t.setAttribute("img-right",this.imgRight),t.setAttribute("img-right-link",this.imgRightLink),t.setAttribute("align-v",this.alignVertical),t.setAttribute("config-path",this.configPath),t.setAttribute("to-selector",""),t.setAttribute("from-selector",""),this.setVerticalAlignment(t),e.append(t)}}},this._injectFromSelector=()=>{const t=document.querySelector(this.fromSelector);t&&(t.getAttribute("main-title")&&(this.textCenter=`<b>${t.getAttribute("main-title")}</b>`),t.getAttribute("author")&&(this.textCenter+=` &ndash; <i>${t.getAttribute("author")}</i>`),t.getAttribute("date")&&(this.textCenter+=` &ndash; ${t.getAttribute("date")}`),t.getAttribute("url-logo")&&(this.imgLeftLink=t.getAttribute("url-logo")),t.getAttribute("img-src")&&(this.imgLeft=t.getAttribute("img-src")),t.getAttribute("footer-text-left")&&(this.textLeft=t.getAttribute("footer-text-left")),t.getAttribute("footer-img-left")&&(this.imgLeft=t.getAttribute("footer-img-left")),t.getAttribute("footer-img-left-link")&&(this.imgLeftLink=t.getAttribute("footer-img-left-link")),t.getAttribute("footer-text-center")&&(this.textCenter=t.getAttribute("footer-text-center")),t.getAttribute("footer-img-center")&&(this.imgCenter=t.getAttribute("footer-img-center")),t.getAttribute("footer-img-center-link")&&(this.imgCenterLink=t.getAttribute("footer-img-center-link")),t.getAttribute("footer-text-right")&&(this.textRight=t.getAttribute("footer-text-right")),t.getAttribute("footer-img-right")&&(this.imgRight=t.getAttribute("footer-img-right")),t.getAttribute("footer-img-right-link")&&(this.imgRightLink=t.getAttribute("footer-img-right-link")),t.getAttribute("footer-align-v")&&(this.alignVertical=t.getAttribute("footer-align-v")))}}makeFooter(){let t="none",e="none",i="none";return this.imgLeft&&(t="inline"),this.imgCenter&&(e="inline"),this.imgRight&&(i="inline"),`\n      <div class="dd-footer">\n        <div class="dd-footer-item dd-footer-left">\n            <a class="footer-link" style="display:${t};"\n                                   href="${this.imgLeftLink}"\n                                   target="_blank">\n              <img class="footer-img footer-img-left"  src="${this.imgLeft}" alt="imgLeft"></a>\n            <div class="footer-text" style="display:inline-block;">${this.textLeft}</div>\n        </div>\n\n        <div class="dd-footer-item dd-footer-center">\n            <a class="footer-link" style="display:${e};"\n                                   href="${this.imgCenterLink}"\n                                   target="_blank">\n              <img class="footer-img" footer-img-center" src="${this.imgCenter}" alt="imgCenter">\n            </a>\n            <div class="footer-text" style="display:inline-block;">${this.textCenter}</div>\n        </div>\n\n        <div class="dd-footer-item dd-footer-right">\n            <div class="footer-text" style="display:inline-block;">${this.textRight}</div>\n            <a class="footer-link" style="display:${i};"\n                                   href="${this.imgRightLink}"\n                                   target="_blank">\n              <img class="footer-img footer-img-right" src="${this.imgRight}" alt="imgRight">\n            </a>\n        </div>\n\n      </div>\n    `}async setPropsFromJson(){const t=await async function(t){if(await(async t=>404!==(await fetch(t,{method:"HEAD"})).status||(console.error(`JSON config does not exist at '${t}'`),!1))(t))try{const e=await fetch(t);return await e.json()}catch(e){console.error(`Error while reading config file at ${t} \n\n ${e}`)}return{error:"Could not parse JSON config, see console for errors"}}(this.configPath);t.error?this.textCenter=`<i><b>[ERROR]</b>${t.error} </i>`:(this.textCenter||(t.title&&(this.textCenter=`<b>${t.title}</b>`),t.mainTitle&&(this.textCenter=`<b>${t.mainTitle}</b>`),t.author&&(this.textCenter+=` &ndash; <i>${t.author}</i>`),t.date&&(this.textCenter+=` &ndash; ${t.date}`)),t.urlLogo&&!this.imgLeftLink&&(this.imgLeftLink=t.urlLogo),t.imgSrc&&!this.imgLeft&&(this.imgLeft=t.imgSrc))}setVerticalAlignment(t){"center"===this.alignVertical?(t.style.setProperty("--footer-align-v","center"),t.style.setProperty("--footer-align-flex-v","center")):"top"===this.alignVertical?(t.style.setProperty("--footer-align-v","start"),t.style.setProperty("--footer-align-flex-v","flex-start")):"bottom"===this.alignVertical&&(t.style.setProperty("--footer-align-v","end"),t.style.setProperty("--footer-align-flex-v","flex-end"))}connectedCallback(){super.connectedCallback(),document.addEventListener("DOMContentLoaded",this._injectFromSelector),document.addEventListener("DOMContentLoaded",this._injectIntoSelector)}disconnectedCallback(){window.removeEventListener("DOMContentLoaded",this._injectIntoSelector),window.removeEventListener("DOMContentLoaded",this._injectFromSelector),super.disconnectedCallback()}render(){let t="";return this.setVerticalAlignment(this),this.configPath&&this.setPropsFromJson(),t+=this.makeFooter(),hs`${Ms(t)}`}}function Zs(t,e,i,s){var o,r=arguments.length,n=r<3?e:null===s?s=Object.getOwnPropertyDescriptor(e,i):s;if("object"==typeof Reflect&&"function"==typeof Reflect.decorate)n=Reflect.decorate(t,e,i,s);else for(var l=t.length-1;l>=0;l--)(o=t[l])&&(n=(r<3?o(n):r>3?o(e,i,n):o(e,i))||n);return r>3&&n&&Object.defineProperty(e,i,n),n
/**
 * @license
 * Copyright 2019 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}Gs.styles=((t,...e)=>{const i=1===t.length?t[0]:e.reduce(((e,i,s)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(i)+t[s+1]),t[0]);return new Oi(i,Mi)})`
    :host {
      --footer-height: var(--dd-footer-height, 40px);
      --footer-img-height: var(--dd-footer-img-height, var(--dd-footer-height));
      --footer-align-v: var(--dd-footer-align-v, center);
      --footer-align-flex-v: var(--dd-footer-align-flex-v, center);
      --footer-padding-side: var(--dd-footer-padding-side, 0px);
      --footer-padding-bottom: var(--dd-footer-padding-bottom, 0px);
      --footer-padding-text: var(--dd-footer-padding-text, 0 2px 0 2px);
      --footer-font-size: var(--dd-footer-font-size, 16px);
      --footer-bottom: var(--dd-footer-bottom, var(--progress-size));
      --footer-color-bg: var(--dd-footer-color-bg);
    }

    .footer-link {
      z-index: 10;
    }

    .dd-footer {
      width: 100%;
      position: absolute;
      padding-bottom: var(--footer-bottom);
      bottom: 0;
      left: 0;
      display: grid;
      grid-template-areas: 'left center right';
      grid-template-columns: 1fr auto 1fr;
      align-items: var(--footer-align-v);
      height: var(--footer-height);
      justify-content: space-between;
      z-index: 10;
      font-size: var(--footer-font-size);
      background-color: var(--footer-color-bg);
    }

    .dd-footer-item {
      display: flex;
    }

    .dd-footer-left {
      grid-area: left;
      padding-left: var(--footer-padding-side);
      padding-bottom: var(--footer-padding-bottom);
      text-align: left;
      align-items: center;
    }

    .dd-footer-center {
      grid-area: center;
      padding-bottom: var(--footer-padding-bottom);
      text-align: center;
    }

    .dd-footer-right {
      grid-area: right;
      padding-bottom: var(--footer-padding-bottom);
      padding-right: var(--footer-padding-side);
      text-align: right;
      justify-content: flex-end;
    }

    .footer-text {
      align-self: var(--footer-align-flex-v);
      padding: var(--footer-padding-text);
    }

    img.footer-img {
      height: var(--footer-img-height, var(--footer-height));
      display: block;
    }
  `,Ri([ks({type:String,attribute:"text-left"})],Gs.prototype,"textLeft",void 0),Ri([ks({type:String,attribute:"img-left"})],Gs.prototype,"imgLeft",void 0),Ri([ks({type:String,attribute:"img-left-link"})],Gs.prototype,"imgLeftLink",void 0),Ri([ks({type:String,attribute:"text-center"})],Gs.prototype,"textCenter",void 0),Ri([ks({type:String,attribute:"img-center"})],Gs.prototype,"imgCenter",void 0),Ri([ks({type:String,attribute:"img-center-link"})],Gs.prototype,"imgCenterLink",void 0),Ri([ks({type:String,attribute:"text-right"})],Gs.prototype,"textRight",void 0),Ri([ks({type:String,attribute:"img-right"})],Gs.prototype,"imgRight",void 0),Ri([ks({type:String,attribute:"img-right-link"})],Gs.prototype,"imgRightLink",void 0),Ri([ks({type:String,attribute:"config-path"})],Gs.prototype,"configPath",void 0),Ri([ks({type:String,attribute:"align-v"})],Gs.prototype,"alignVertical",void 0),Ri([ks({type:String,attribute:"to-selector"})],Gs.prototype,"toSelector",void 0),Ri([ks({type:String,attribute:"from-selector"})],Gs.prototype,"fromSelector",void 0),window.customElements.define("dd-footer",Gs);const Qs=window.ShadowRoot&&(void 0===window.ShadyCSS||window.ShadyCSS.nativeShadow)&&"adoptedStyleSheets"in Document.prototype&&"replace"in CSSStyleSheet.prototype,Xs=Symbol(),Ys=new Map;class to{constructor(t,e){if(this._$cssResult$=!0,e!==Xs)throw Error("CSSResult is not constructable. Use `unsafeCSS` or `css` instead.");this.cssText=t}get styleSheet(){let t=Ys.get(this.cssText);return Qs&&void 0===t&&(Ys.set(this.cssText,t=new CSSStyleSheet),t.replaceSync(this.cssText)),t}toString(){return this.cssText}}const eo=Qs?t=>t:t=>t instanceof CSSStyleSheet?(t=>{let e="";for(const i of t.cssRules)e+=i.cssText;return(t=>new to("string"==typeof t?t:t+"",Xs))(e)})(t):t
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */;var io;const so=window.trustedTypes,oo=so?so.emptyScript:"",ro=window.reactiveElementPolyfillSupport,no={toAttribute(t,e){switch(e){case Boolean:t=t?oo:null;break;case Object:case Array:t=null==t?t:JSON.stringify(t)}return t},fromAttribute(t,e){let i=t;switch(e){case Boolean:i=null!==t;break;case Number:i=null===t?null:Number(t);break;case Object:case Array:try{i=JSON.parse(t)}catch(t){i=null}}return i}},lo=(t,e)=>e!==t&&(e==e||t==t),ao={attribute:!0,type:String,converter:no,reflect:!1,hasChanged:lo};class ho extends HTMLElement{constructor(){super(),this._$Et=new Map,this.isUpdatePending=!1,this.hasUpdated=!1,this._$Ei=null,this.o()}static addInitializer(t){var e;null!==(e=this.l)&&void 0!==e||(this.l=[]),this.l.push(t)}static get observedAttributes(){this.finalize();const t=[];return this.elementProperties.forEach(((e,i)=>{const s=this._$Eh(i,e);void 0!==s&&(this._$Eu.set(s,i),t.push(s))})),t}static createProperty(t,e=ao){if(e.state&&(e.attribute=!1),this.finalize(),this.elementProperties.set(t,e),!e.noAccessor&&!this.prototype.hasOwnProperty(t)){const i="symbol"==typeof t?Symbol():"__"+t,s=this.getPropertyDescriptor(t,i,e);void 0!==s&&Object.defineProperty(this.prototype,t,s)}}static getPropertyDescriptor(t,e,i){return{get(){return this[e]},set(s){const o=this[t];this[e]=s,this.requestUpdate(t,o,i)},configurable:!0,enumerable:!0}}static getPropertyOptions(t){return this.elementProperties.get(t)||ao}static finalize(){if(this.hasOwnProperty("finalized"))return!1;this.finalized=!0;const t=Object.getPrototypeOf(this);if(t.finalize(),this.elementProperties=new Map(t.elementProperties),this._$Eu=new Map,this.hasOwnProperty("properties")){const t=this.properties,e=[...Object.getOwnPropertyNames(t),...Object.getOwnPropertySymbols(t)];for(const i of e)this.createProperty(i,t[i])}return this.elementStyles=this.finalizeStyles(this.styles),!0}static finalizeStyles(t){const e=[];if(Array.isArray(t)){const i=new Set(t.flat(1/0).reverse());for(const t of i)e.unshift(eo(t))}else void 0!==t&&e.push(eo(t));return e}static _$Eh(t,e){const i=e.attribute;return!1===i?void 0:"string"==typeof i?i:"string"==typeof t?t.toLowerCase():void 0}o(){var t;this._$Ep=new Promise((t=>this.enableUpdating=t)),this._$AL=new Map,this._$Em(),this.requestUpdate(),null===(t=this.constructor.l)||void 0===t||t.forEach((t=>t(this)))}addController(t){var e,i;(null!==(e=this._$Eg)&&void 0!==e?e:this._$Eg=[]).push(t),void 0!==this.renderRoot&&this.isConnected&&(null===(i=t.hostConnected)||void 0===i||i.call(t))}removeController(t){var e;null===(e=this._$Eg)||void 0===e||e.splice(this._$Eg.indexOf(t)>>>0,1)}_$Em(){this.constructor.elementProperties.forEach(((t,e)=>{this.hasOwnProperty(e)&&(this._$Et.set(e,this[e]),delete this[e])}))}createRenderRoot(){var t;const e=null!==(t=this.shadowRoot)&&void 0!==t?t:this.attachShadow(this.constructor.shadowRootOptions);return((t,e)=>{Qs?t.adoptedStyleSheets=e.map((t=>t instanceof CSSStyleSheet?t:t.styleSheet)):e.forEach((e=>{const i=document.createElement("style"),s=window.litNonce;void 0!==s&&i.setAttribute("nonce",s),i.textContent=e.cssText,t.appendChild(i)}))})(e,this.constructor.elementStyles),e}connectedCallback(){var t;void 0===this.renderRoot&&(this.renderRoot=this.createRenderRoot()),this.enableUpdating(!0),null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostConnected)||void 0===e?void 0:e.call(t)}))}enableUpdating(t){}disconnectedCallback(){var t;null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostDisconnected)||void 0===e?void 0:e.call(t)}))}attributeChangedCallback(t,e,i){this._$AK(t,i)}_$ES(t,e,i=ao){var s,o;const r=this.constructor._$Eh(t,i);if(void 0!==r&&!0===i.reflect){const n=(null!==(o=null===(s=i.converter)||void 0===s?void 0:s.toAttribute)&&void 0!==o?o:no.toAttribute)(e,i.type);this._$Ei=t,null==n?this.removeAttribute(r):this.setAttribute(r,n),this._$Ei=null}}_$AK(t,e){var i,s,o;const r=this.constructor,n=r._$Eu.get(t);if(void 0!==n&&this._$Ei!==n){const t=r.getPropertyOptions(n),l=t.converter,a=null!==(o=null!==(s=null===(i=l)||void 0===i?void 0:i.fromAttribute)&&void 0!==s?s:"function"==typeof l?l:null)&&void 0!==o?o:no.fromAttribute;this._$Ei=n,this[n]=a(e,t.type),this._$Ei=null}}requestUpdate(t,e,i){let s=!0;void 0!==t&&(((i=i||this.constructor.getPropertyOptions(t)).hasChanged||lo)(this[t],e)?(this._$AL.has(t)||this._$AL.set(t,e),!0===i.reflect&&this._$Ei!==t&&(void 0===this._$EC&&(this._$EC=new Map),this._$EC.set(t,i))):s=!1),!this.isUpdatePending&&s&&(this._$Ep=this._$E_())}async _$E_(){this.isUpdatePending=!0;try{await this._$Ep}catch(t){Promise.reject(t)}const t=this.scheduleUpdate();return null!=t&&await t,!this.isUpdatePending}scheduleUpdate(){return this.performUpdate()}performUpdate(){var t;if(!this.isUpdatePending)return;this.hasUpdated,this._$Et&&(this._$Et.forEach(((t,e)=>this[e]=t)),this._$Et=void 0);let e=!1;const i=this._$AL;try{e=this.shouldUpdate(i),e?(this.willUpdate(i),null===(t=this._$Eg)||void 0===t||t.forEach((t=>{var e;return null===(e=t.hostUpdate)||void 0===e?void 0:e.call(t)})),this.update(i)):this._$EU()}catch(t){throw e=!1,this._$EU(),t}e&&this._$AE(i)}willUpdate(t){}_$AE(t){var e;null===(e=this._$Eg)||void 0===e||e.forEach((t=>{var e;return null===(e=t.hostUpdated)||void 0===e?void 0:e.call(t)})),this.hasUpdated||(this.hasUpdated=!0,this.firstUpdated(t)),this.updated(t)}_$EU(){this._$AL=new Map,this.isUpdatePending=!1}get updateComplete(){return this.getUpdateComplete()}getUpdateComplete(){return this._$Ep}shouldUpdate(t){return!0}update(t){void 0!==this._$EC&&(this._$EC.forEach(((t,e)=>this._$ES(e,this[e],t))),this._$EC=void 0),this._$EU()}updated(t){}firstUpdated(t){}}
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var co;ho.finalized=!0,ho.elementProperties=new Map,ho.elementStyles=[],ho.shadowRootOptions={mode:"open"},null==ro||ro({ReactiveElement:ho}),(null!==(io=globalThis.reactiveElementVersions)&&void 0!==io?io:globalThis.reactiveElementVersions=[]).push("1.3.2");const po=globalThis.trustedTypes,uo=po?po.createPolicy("lit-html",{createHTML:t=>t}):void 0,go=`lit$${(Math.random()+"").slice(9)}$`,vo="?"+go,fo=`<${vo}>`,$o=document,mo=(t="")=>$o.createComment(t),yo=t=>null===t||"object"!=typeof t&&"function"!=typeof t,_o=Array.isArray,Ao=/<(?:(!--|\/[^a-zA-Z])|(\/?[a-zA-Z][^>\s]*)|(\/?$))/g,bo=/-->/g,wo=/>/g,So=/>|[ 	\n\r](?:([^\s"'>=/]+)([ 	\n\r]*=[ 	\n\r]*(?:[^ 	\n\r"'`<>=]|("|')|))|$)/g,Eo=/'/g,xo=/"/g,Co=/^(?:script|style|textarea|title)$/i,To=(t=>(e,...i)=>({_$litType$:t,strings:e,values:i}))(1),Po=Symbol.for("lit-noChange"),Uo=Symbol.for("lit-nothing"),ko=new WeakMap,Ho=$o.createTreeWalker($o,129,null,!1),Ro=(t,e)=>{const i=t.length-1,s=[];let o,r=2===e?"<svg>":"",n=Ao;for(let e=0;e<i;e++){const i=t[e];let l,a,d=-1,h=0;for(;h<i.length&&(n.lastIndex=h,a=n.exec(i),null!==a);)h=n.lastIndex,n===Ao?"!--"===a[1]?n=bo:void 0!==a[1]?n=wo:void 0!==a[2]?(Co.test(a[2])&&(o=RegExp("</"+a[2],"g")),n=So):void 0!==a[3]&&(n=So):n===So?">"===a[0]?(n=null!=o?o:Ao,d=-1):void 0===a[1]?d=-2:(d=n.lastIndex-a[2].length,l=a[1],n=void 0===a[3]?So:'"'===a[3]?xo:Eo):n===xo||n===Eo?n=So:n===bo||n===wo?n=Ao:(n=So,o=void 0);const c=n===So&&t[e+1].startsWith("/>")?" ":"";r+=n===Ao?i+fo:d>=0?(s.push(l),i.slice(0,d)+"$lit$"+i.slice(d)+go+c):i+go+(-2===d?(s.push(void 0),e):c)}const l=r+(t[i]||"<?>")+(2===e?"</svg>":"");if(!Array.isArray(t)||!t.hasOwnProperty("raw"))throw Error("invalid template strings array");return[void 0!==uo?uo.createHTML(l):l,s]};class Lo{constructor({strings:t,_$litType$:e},i){let s;this.parts=[];let o=0,r=0;const n=t.length-1,l=this.parts,[a,d]=Ro(t,e);if(this.el=Lo.createElement(a,i),Ho.currentNode=this.el.content,2===e){const t=this.el.content,e=t.firstChild;e.remove(),t.append(...e.childNodes)}for(;null!==(s=Ho.nextNode())&&l.length<n;){if(1===s.nodeType){if(s.hasAttributes()){const t=[];for(const e of s.getAttributeNames())if(e.endsWith("$lit$")||e.startsWith(go)){const i=d[r++];if(t.push(e),void 0!==i){const t=s.getAttribute(i.toLowerCase()+"$lit$").split(go),e=/([.?@])?(.*)/.exec(i);l.push({type:1,index:o,name:e[2],strings:t,ctor:"."===e[1]?Do:"?"===e[1]?jo:"@"===e[1]?Io:zo})}else l.push({type:6,index:o})}for(const e of t)s.removeAttribute(e)}if(Co.test(s.tagName)){const t=s.textContent.split(go),e=t.length-1;if(e>0){s.textContent=po?po.emptyScript:"";for(let i=0;i<e;i++)s.append(t[i],mo()),Ho.nextNode(),l.push({type:2,index:++o});s.append(t[e],mo())}}}else if(8===s.nodeType)if(s.data===vo)l.push({type:2,index:o});else{let t=-1;for(;-1!==(t=s.data.indexOf(go,t+1));)l.push({type:7,index:o}),t+=go.length-1}o++}}static createElement(t,e){const i=$o.createElement("template");return i.innerHTML=t,i}}function Mo(t,e,i=t,s){var o,r,n,l;if(e===Po)return e;let a=void 0!==s?null===(o=i._$Cl)||void 0===o?void 0:o[s]:i._$Cu;const d=yo(e)?void 0:e._$litDirective$;return(null==a?void 0:a.constructor)!==d&&(null===(r=null==a?void 0:a._$AO)||void 0===r||r.call(a,!1),void 0===d?a=void 0:(a=new d(t),a._$AT(t,i,s)),void 0!==s?(null!==(n=(l=i)._$Cl)&&void 0!==n?n:l._$Cl=[])[s]=a:i._$Cu=a),void 0!==a&&(e=Mo(t,a._$AS(t,e.values),a,s)),e}class No{constructor(t,e){this.v=[],this._$AN=void 0,this._$AD=t,this._$AM=e}get parentNode(){return this._$AM.parentNode}get _$AU(){return this._$AM._$AU}p(t){var e;const{el:{content:i},parts:s}=this._$AD,o=(null!==(e=null==t?void 0:t.creationScope)&&void 0!==e?e:$o).importNode(i,!0);Ho.currentNode=o;let r=Ho.nextNode(),n=0,l=0,a=s[0];for(;void 0!==a;){if(n===a.index){let e;2===a.type?e=new Oo(r,r.nextSibling,this,t):1===a.type?e=new a.ctor(r,a.name,a.strings,this,t):6===a.type&&(e=new Vo(r,this,t)),this.v.push(e),a=s[++l]}n!==(null==a?void 0:a.index)&&(r=Ho.nextNode(),n++)}return o}m(t){let e=0;for(const i of this.v)void 0!==i&&(void 0!==i.strings?(i._$AI(t,i,e),e+=i.strings.length-2):i._$AI(t[e])),e++}}class Oo{constructor(t,e,i,s){var o;this.type=2,this._$AH=Uo,this._$AN=void 0,this._$AA=t,this._$AB=e,this._$AM=i,this.options=s,this._$Cg=null===(o=null==s?void 0:s.isConnected)||void 0===o||o}get _$AU(){var t,e;return null!==(e=null===(t=this._$AM)||void 0===t?void 0:t._$AU)&&void 0!==e?e:this._$Cg}get parentNode(){let t=this._$AA.parentNode;const e=this._$AM;return void 0!==e&&11===t.nodeType&&(t=e.parentNode),t}get startNode(){return this._$AA}get endNode(){return this._$AB}_$AI(t,e=this){t=Mo(this,t,e),yo(t)?t===Uo||null==t||""===t?(this._$AH!==Uo&&this._$AR(),this._$AH=Uo):t!==this._$AH&&t!==Po&&this.$(t):void 0!==t._$litType$?this.T(t):void 0!==t.nodeType?this.k(t):(t=>{var e;return _o(t)||"function"==typeof(null===(e=t)||void 0===e?void 0:e[Symbol.iterator])})(t)?this.S(t):this.$(t)}M(t,e=this._$AB){return this._$AA.parentNode.insertBefore(t,e)}k(t){this._$AH!==t&&(this._$AR(),this._$AH=this.M(t))}$(t){this._$AH!==Uo&&yo(this._$AH)?this._$AA.nextSibling.data=t:this.k($o.createTextNode(t)),this._$AH=t}T(t){var e;const{values:i,_$litType$:s}=t,o="number"==typeof s?this._$AC(t):(void 0===s.el&&(s.el=Lo.createElement(s.h,this.options)),s);if((null===(e=this._$AH)||void 0===e?void 0:e._$AD)===o)this._$AH.m(i);else{const t=new No(o,this),e=t.p(this.options);t.m(i),this.k(e),this._$AH=t}}_$AC(t){let e=ko.get(t.strings);return void 0===e&&ko.set(t.strings,e=new Lo(t)),e}S(t){_o(this._$AH)||(this._$AH=[],this._$AR());const e=this._$AH;let i,s=0;for(const o of t)s===e.length?e.push(i=new Oo(this.M(mo()),this.M(mo()),this,this.options)):i=e[s],i._$AI(o),s++;s<e.length&&(this._$AR(i&&i._$AB.nextSibling,s),e.length=s)}_$AR(t=this._$AA.nextSibling,e){var i;for(null===(i=this._$AP)||void 0===i||i.call(this,!1,!0,e);t&&t!==this._$AB;){const e=t.nextSibling;t.remove(),t=e}}setConnected(t){var e;void 0===this._$AM&&(this._$Cg=t,null===(e=this._$AP)||void 0===e||e.call(this,t))}}class zo{constructor(t,e,i,s,o){this.type=1,this._$AH=Uo,this._$AN=void 0,this.element=t,this.name=e,this._$AM=s,this.options=o,i.length>2||""!==i[0]||""!==i[1]?(this._$AH=Array(i.length-1).fill(new String),this.strings=i):this._$AH=Uo}get tagName(){return this.element.tagName}get _$AU(){return this._$AM._$AU}_$AI(t,e=this,i,s){const o=this.strings;let r=!1;if(void 0===o)t=Mo(this,t,e,0),r=!yo(t)||t!==this._$AH&&t!==Po,r&&(this._$AH=t);else{const s=t;let n,l;for(t=o[0],n=0;n<o.length-1;n++)l=Mo(this,s[i+n],e,n),l===Po&&(l=this._$AH[n]),r||(r=!yo(l)||l!==this._$AH[n]),l===Uo?t=Uo:t!==Uo&&(t+=(null!=l?l:"")+o[n+1]),this._$AH[n]=l}r&&!s&&this.C(t)}C(t){t===Uo?this.element.removeAttribute(this.name):this.element.setAttribute(this.name,null!=t?t:"")}}class Do extends zo{constructor(){super(...arguments),this.type=3}C(t){this.element[this.name]=t===Uo?void 0:t}}const Bo=po?po.emptyScript:"";class jo extends zo{constructor(){super(...arguments),this.type=4}C(t){t&&t!==Uo?this.element.setAttribute(this.name,Bo):this.element.removeAttribute(this.name)}}class Io extends zo{constructor(t,e,i,s,o){super(t,e,i,s,o),this.type=5}_$AI(t,e=this){var i;if((t=null!==(i=Mo(this,t,e,0))&&void 0!==i?i:Uo)===Po)return;const s=this._$AH,o=t===Uo&&s!==Uo||t.capture!==s.capture||t.once!==s.once||t.passive!==s.passive,r=t!==Uo&&(s===Uo||o);o&&this.element.removeEventListener(this.name,this,s),r&&this.element.addEventListener(this.name,this,t),this._$AH=t}handleEvent(t){var e,i;"function"==typeof this._$AH?this._$AH.call(null!==(i=null===(e=this.options)||void 0===e?void 0:e.host)&&void 0!==i?i:this.element,t):this._$AH.handleEvent(t)}}class Vo{constructor(t,e,i){this.element=t,this.type=6,this._$AN=void 0,this._$AM=e,this.options=i}get _$AU(){return this._$AM._$AU}_$AI(t){Mo(this,t)}}const Wo=window.litHtmlPolyfillSupport;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
var qo,Fo;null==Wo||Wo(Lo,Oo),(null!==(co=globalThis.litHtmlVersions)&&void 0!==co?co:globalThis.litHtmlVersions=[]).push("2.2.6");class Jo extends ho{constructor(){super(...arguments),this.renderOptions={host:this},this._$Dt=void 0}createRenderRoot(){var t,e;const i=super.createRenderRoot();return null!==(t=(e=this.renderOptions).renderBefore)&&void 0!==t||(e.renderBefore=i.firstChild),i}update(t){const e=this.render();this.hasUpdated||(this.renderOptions.isConnected=this.isConnected),super.update(t),this._$Dt=((t,e,i)=>{var s,o;const r=null!==(s=null==i?void 0:i.renderBefore)&&void 0!==s?s:e;let n=r._$litPart$;if(void 0===n){const t=null!==(o=null==i?void 0:i.renderBefore)&&void 0!==o?o:null;r._$litPart$=n=new Oo(e.insertBefore(mo(),t),t,void 0,null!=i?i:{})}return n._$AI(t),n})(e,this.renderRoot,this.renderOptions)}connectedCallback(){var t;super.connectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!0)}disconnectedCallback(){var t;super.disconnectedCallback(),null===(t=this._$Dt)||void 0===t||t.setConnected(!1)}render(){return Po}}Jo.finalized=!0,Jo._$litElement$=!0,null===(qo=globalThis.litElementHydrateSupport)||void 0===qo||qo.call(globalThis,{LitElement:Jo});const Ko=globalThis.litElementPolyfillSupport;null==Ko||Ko({LitElement:Jo}),(null!==(Fo=globalThis.litElementVersions)&&void 0!==Fo?Fo:globalThis.litElementVersions=[]).push("3.2.0");
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const Go=(t,e)=>"method"===e.kind&&e.descriptor&&!("value"in e.descriptor)?{...e,finisher(i){i.createProperty(e.key,t)}}:{kind:"field",key:Symbol(),placement:"own",descriptor:{},originalKey:e.key,initializer(){"function"==typeof e.initializer&&(this[e.key]=e.initializer.call(this))},finisher(i){i.createProperty(e.key,t)}};function Zo(t){return(e,i)=>void 0!==i?((t,e,i)=>{e.constructor.createProperty(i,t)})(t,e,i):Go(t,e)
/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */}var Qo;null===(Qo=window.HTMLSlotElement)||void 0===Qo||Qo.prototype.assignedElements;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
const Xo=2;
/**
 * @license
 * Copyright 2017 Google LLC
 * SPDX-License-Identifier: BSD-3-Clause
 */
class Yo extends class{constructor(t){}get _$AU(){return this._$AM._$AU}_$AT(t,e,i){this._$Ct=t,this._$AM=e,this._$Ci=i}_$AS(t,e){return this.update(t,e)}update(t,e){return this.render(...e)}}{constructor(t){if(super(t),this.it=Uo,t.type!==Xo)throw Error(this.constructor.directiveName+"() can only be used in child bindings")}render(t){if(t===Uo||null==t)return this.ft=void 0,this.it=t;if(t===Po)return t;if("string"!=typeof t)throw Error(this.constructor.directiveName+"() called with a non-string value");if(t===this.it)return this.ft;this.it=t;const e=[t];return e.raw=e,this.ft={_$litType$:this.constructor.resultType,strings:e,values:[]}}}Yo.directiveName="unsafeHTML",Yo.resultType=1;const tr=(t=>(...e)=>({_$litDirective$:t,values:e}))(Yo),er="",ir="",sr="",or="",rr="",nr="",lr="",ar="dd-slide-collection",dr="",hr=!1,cr=!1,pr=!1,ur="",gr="",vr="",fr="",$r="",mr="",yr="100%";class _r extends Jo{constructor(){super(...arguments),this.imgSrc=sr,this.mainTitle=er,this.subTitle=ir,this.author=or,this.organisation=rr,this.organisationUrl=nr,this.date=lr,this.noDefaultMap=hr,this.centerText=cr,this.centerImg=pr,this.configPath=dr,this.fromSelector=ar,this.htmlTopLeft=ur,this.htmlTopRight=gr,this.htmlMidLeft=vr,this.htmlMidRight=fr,this.htmlBotLeft=$r,this.htmlBotRight=mr,this.widthLeft=yr}makeTitlePage(){return`\n        <div class='dd-titlepage'>\n\n          \x3c!-- logo --\x3e\n          <div id="dd-titlepage-logo">\n            <img src="${this.imgSrc}" alt="dd-logo";>\n          </div>\n\n          \x3c!-- top --\x3e\n          <div class="dd-titlepage-top-l dd-titlepage-top">\n              ${this.htmlTopLeft}\n          </div>\n          <div class="dd-titlepage-top-r dd-titlepage-top">\n              ${this.htmlTopRight}\n          </div>\n\n          \x3c!-- middle --\x3e\n          <div class="dd-titlepage-mid-l dd-titlepage-middle">\n            ${this.htmlMidLeft}\n          </div>\n          <div class="dd-titlepage-mid-r dd-titlepage-middle">\n            ${this.htmlMidRight}\n          </div>\n\n          \x3c!-- bottom --\x3e\n          <div class="dd-titlepage-bot-l dd-titlepage-bottom">\n            ${this.htmlBotLeft}\n          </div>\n          <div class="dd-titlepage-bot-r dd-titlepage-bottom">\n            ${this.htmlBotRight}\n          </div>\n        </div>\n      `}_mapDefault(){(this.mainTitle||this.subTitle)&&(this.htmlMidLeft=`\n        <div class="dd-titlepage-title dd-titlepage-mid-l default">\n          <strong>${this.mainTitle}</strong>\n        </div>\n        <div class="dd-titlepage-subtitle dd-titlepage-mid-l default">\n          ${this.subTitle}\n        </div>`),(this.author||this.organisation||this.date)&&(this.organisationUrl?this.htmlBotLeft=`\n          <div class="dd-titlepage-bot-l default">\n          <strong>${this.author}</strong>  <br>\n          <a class="dd-titlepage-org-url" href="${this.organisationUrl}">${this.organisation}</a> <br>\n          ${this.date}\n        </div>`:this.htmlBotLeft=`\n          <div class="dd-titlepage-bot-l default">\n          <strong>${this.author}</strong>  <br>\n                  ${this.organisation}     <br>\n                  ${this.date}\n        </div>`)}async setPropsFromJson(){const t=await async function(t){if(await(async t=>404!==(await fetch(t,{method:"HEAD"})).status||(console.error(`JSON config does not exist at '${t}'`),!1))(t))try{const e=await fetch(t);return await e.json()}catch(e){console.error(`Error while reading config file at ${t} \n\n ${e}`)}return{error:"Could not parse JSON config, see console for errors"}}(this.configPath);t.error?this.htmlMidLeft=`<i><b>[ERROR]</b>${t.error} </i>`:(t.title&&(this.mainTitle=t.title),t.mainTitle&&(this.mainTitle=t.mainTitle),t.subTitle&&(this.subTitle=t.subTitle),t.author&&(this.author=t.author),t.organisation&&(this.organisation=t.organisation),t.date&&(this.date=t.date),t.imgSrc&&(this.imgSrc=t.imgSrc))}injectFromSelector(){const t=document.querySelector(this.fromSelector);t&&(t.getAttribute("main-title")&&(this.mainTitle=t.getAttribute("main-title")),t.getAttribute("sub-title")&&(this.subTitle=t.getAttribute("sub-title")),t.getAttribute("author")&&(this.author=t.getAttribute("author")),t.getAttribute("date")&&(this.date=t.getAttribute("date")),t.getAttribute("organisation")&&(this.organisation=t.getAttribute("organisation")),t.getAttribute("organisation-url")&&(this.organisationUrl=t.getAttribute("organisation-url")),t.getAttribute("img-src")&&(this.imgSrc=t.getAttribute("img-src")))}async firstUpdated(){if(!this.imgSrc){this.shadowRoot.querySelector("#dd-titlepage-logo").style.display="none"}this.centerText&&(this.style.setProperty("--titlepage-align-lsec","center"),this.style.setProperty("--titlepage-padding-left","0px"),this.style.setProperty("--titlepage-padding-right","0px"),this.style.setProperty("--titlepage-w-left","100%")),this.widthLeft!==yr&&this.style.setProperty("--titlepage-w-left",this.widthLeft),this.centerImg&&window.addEventListener("load",(()=>{const t=this.shadowRoot.querySelector("#dd-titlepage-logo img").clientWidth;this.style.setProperty("--titlepage-logo-left",`calc( 50% - ${t/2}px )`)}))}render(){let t="";return this.title="Titlepage",this.configPath&&this.setPropsFromJson(),this.classList.add("slide"),this.classList.add("titlepage"),this.fromSelector&&this.injectFromSelector(),this.noDefaultMap||this._mapDefault(),t+=this.makeTitlePage(),To`${tr(t)}`}}_r.styles=((t,...e)=>{const i=1===t.length?t[0]:e.reduce(((e,i,s)=>e+(t=>{if(!0===t._$cssResult$)return t.cssText;if("number"==typeof t)return t;throw Error("Value passed to 'css' function must be a 'css' function result: "+t+". Use 'unsafeCSS' to pass non-literal values, but take care to ensure page security.")})(i)+t[s+1]),t[0]);return new to(i,Xs)})`
    :host {
      /* slide placeholders */
      --slide-ratio: var(--dd-slide-ratio, calc(16 / 9));
      --slide-width: var(--dd-slide-width, 1024px);
      --slide-height: var(
        --dd-slide-height,
        calc(var(--slide-width) / var(--slide-ratio))
      );
      --titlepage-font-size: var(--dd-titlepage-font-size, 24px);
      --titlepage-font: var(--dd-font, 24px/2 'Roboto', sans-serif);

      /* titlepage */
      --titlepage-padding-side: var(--dd-titlepage-padding-side, 50px);
      --titlepage-padding-left: var(--titlepage-padding-side);
      --titlepage-padding-right: var(--titlepage-padding-side);
      --titlepage-padding-top-top: var(--dd-titlepage-padding-top-top, 10px);
      --titlepage-padding-mid-top: var(--dd-titlepage-padding-mid-top, 100px);
      --titlepage-padding-bot-top: var(--dd-titlepage-padding-bot-top, 10px);
      --titlepage-padding-sectop: var(--titlepage-padding-top-top)
        var(--titlepage-padding-right) 0 var(--titlepage-padding-left);
      --titlepage-padding-secmid: var(--titlepage-padding-mid-top)
        var(--titlepage-padding-right) 0 var(--titlepage-padding-left);
      --titlepage-padding-secbot: var(--titlepage-padding-bot-top)
        var(--titlepage-padding-right) 0 var(--titlepage-padding-left);

      --titlepage-align-lsec: var(--dd-titlepage-align-lsec, left);
      --titlepage-align-rsec: var(--dd-titlepage-align-rsec, right);

      --titlepage-w-left: var(--dd-titlepage-w-left, 100%);
      --titlepage-h-top: var(
        --dd-titlepage-h-top,
        calc(0.15 * var(--slide-height))
      );
      --titlepage-h-middle: var(
        --dd-titlepage-h-middle,
        calc(
          var(--slide-height) - var(--titlepage-h-top) -
            var(--titlepage-h-bottom)
        )
      );
      --titlepage-h-bottom: var(
        --dd-titlepage-h-bottom,
        calc(0.2 * var(--slide-height))
      );

      --titlepage-title-font-size: var(
        --dd-titlepage-title-font-size,
        calc(2.15 * var(--titlepage-font-size))
      );
      --titlepage-subtitle-font-size: var(
        --dd-titlepage-subtitle-font-size,
        calc(0.6 * var(--titlepage-title-font-size))
      );

      --titlepage-logo-height: var(
        --dd-titlepage-logo-height,
        calc(var(--titlepage-h-top) / 1.3)
      );
      --titlepage-logo-top: var(
        --dd-titlepage-logo-top,
        calc(var(--titlepage-h-top) - var(--titlepage-logo-height) / 2)
      );
      --titlepage-logo-left: var(
        --dd-titlepage-logo-left,
        var(--titlepage-padding-side)
      );

      /* dd color pallette */
      --titlepage-prim-color: var(--dd-prim-color, rgba(153, 155, 132, 1));
      --titlepage-prim-color: var(--dd-prim-color, rgba(121, 135, 119, 1));
      /*

      */
      --titlepage-prim-color-dark: var(
        --dd-prim-color-dark,
        rgba(65, 90, 72, 1)
      );
      --titlepage-color-sec: var(--dd-color-sec, rgba(248, 237, 227, 1));
      --titlepage-color-sec-dark: var(
        --dd-color-sec-dark,
        rgba(238, 254, 216, 1)
      );
      --titlepage-color-text: var(--dd-color-text, rgba(0, 0, 0, 0.9));
      --titlepage-color-text-light: var(
        --dd-color-text-light,
        rgba(255, 255, 255, 1)
      );

      font: var(--titlepage-font);
      font-size: var(--titlepage-font-size);

      display: block;
    }

    :host(.slide) {
      position: relative;
      z-index: 0;
      overflow: hidden;
      box-sizing: border-box;
      width: var(--slide-width);
      height: var(--slide-height);
      background-color: white;
      max-width: 100%;
    }

    .dd-titlepage {
      min-height: 100%;
      height: 100%;
      width: 100%;
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      margin: 0;
      display: grid;
      grid-template-areas:
        'top-l top-r'
        'mid-l mid-r'
        'bot-l bot-r';
      grid-template-columns: var(--titlepage-w-left) 1fr;
      grid-template-rows: var(--titlepage-h-top) auto var(--titlepage-h-bottom);
      align-items: top;
      justify-content: space-between;
      /* make sure slide number is not shown */
      z-index: 5;
    }

    #dd-titlepage-logo {
      position: absolute;
      top: var(--titlepage-logo-top);
      left: var(--titlepage-logo-left);
    }

    #dd-titlepage-logo img {
      height: var(--titlepage-logo-height);
      width: auto;
    }

    .dd-titlepage-top-l {
      grid-area: top-l;
      text-align: var(--titlepage-align-lsec);
    }
    .dd-titlepage-top-r {
      grid-area: top-r;
      text-align: var(--titlepage-align-rsec);
    }

    .dd-titlepage-mid-l {
      text-align: var(--titlepage-align-lsec);
      grid-area: mid-l;
    }
    .dd-titlepage-mid-r {
      grid-area: mid-r;
      text-align: var(--titlepage-align-rsec);
    }

    .dd-titlepage-bot-l {
      grid-area: bot-l;
      text-align: var(--titlepage-align-lsec);
    }
    .dd-titlepage-bot-r {
      grid-area: bot-r;
      text-align: var(--titlepage-align-rsec);
    }

    .dd-titlepage-top {
      top: 0;
      background-color: var(--titlepage-color-sec);
      color: var(--titlepage-color-text);
      padding: var(--titlepage-padding-sectop);
    }

    .dd-titlepage-middle {
      max-height: var(--titlepage-h-middle);
      background-color: var(--titlepage-prim-color);
      color: var(--titlepage-color-text-light);
      padding: var(--titlepage-padding-secmid);
    }

    .dd-titlepage-bottom {
      padding: var(--titlepage-padding-secbot);
      background-color: var(--titlepage-color-sec);
      color: var(--titlepage-color-text);
    }
    .dd-titlepage-bottom .default {
      font-size: calc(0.45 * var(--titlepage-title-font-size));
      line-height: 1.3em;
    }

    .dd-titlepage-title {
      font-size: var(--titlepage-title-font-size);
      line-height: 1.3em;
    }

    .dd-titlepage-subtitle {
      padding-top: 0.8em;
      font-size: var(--titlepage-subtitle-font-size);
      line-height: 1.3em;
    }

    .dd-titlepage-org-url {
      text-decoration: none;
      /*color: var(--titlepage-color-text)*/
    }

    @media (max-width: 1168px) {
      :host {
        --titlepage-title-font-size: calc(1.8 * var(--titlepage-font-size));
        --slide-width: 1000px;
      }
    }
    @media (max-width: 700px) {
      :host {
        --titlepage-title-font-size: calc(1.2 * var(--titlepage-font-size));
        --slide-width: 600px;
      }
    }
  `,Zs([Zo({type:String,attribute:"img-src"})],_r.prototype,"imgSrc",void 0),Zs([Zo({type:String,attribute:"main-title"})],_r.prototype,"mainTitle",void 0),Zs([Zo({type:String,attribute:"sub-title"})],_r.prototype,"subTitle",void 0),Zs([Zo({type:String,attribute:"author"})],_r.prototype,"author",void 0),Zs([Zo({type:String,attribute:"organisation"})],_r.prototype,"organisation",void 0),Zs([Zo({type:String,attribute:"organisation-url"})],_r.prototype,"organisationUrl",void 0),Zs([Zo({type:String,attribute:"date"})],_r.prototype,"date",void 0),Zs([Zo({type:Boolean,attribute:"no-default-map"})],_r.prototype,"noDefaultMap",void 0),Zs([Zo({type:Boolean,attribute:"center-text"})],_r.prototype,"centerText",void 0),Zs([Zo({type:Boolean,attribute:"center-img"})],_r.prototype,"centerImg",void 0),Zs([Zo({type:String,attribute:"config-path"})],_r.prototype,"configPath",void 0),Zs([Zo({type:String,attribute:"from-selector"})],_r.prototype,"fromSelector",void 0),Zs([Zo({type:String,attribute:"html-top-left"})],_r.prototype,"htmlTopLeft",void 0),Zs([Zo({type:String,attribute:"html-top-right"})],_r.prototype,"htmlTopRight",void 0),Zs([Zo({type:String,attribute:"html-mid-left"})],_r.prototype,"htmlMidLeft",void 0),Zs([Zo({type:String,attribute:"html-mid-right"})],_r.prototype,"htmlMidRight",void 0),Zs([Zo({type:String,attribute:"html-bot-left"})],_r.prototype,"htmlBotLeft",void 0),Zs([Zo({type:String,attribute:"html-bot-right"})],_r.prototype,"htmlBotRight",void 0),Zs([Zo({type:String,attribute:"width-left"})],_r.prototype,"widthLeft",void 0),window.customElements.define("dd-titlepage",_r);
